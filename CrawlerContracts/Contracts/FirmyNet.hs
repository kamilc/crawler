{-# LANGUAGE TypeFamilies, FlexibleInstances, MultiParamTypeClasses #-}
module Contracts.FirmyNet where

import Data.ByteString.UTF8 as UTF8
import Control.Concurrent.STM
import Control.Concurrent.STM.TVar
import WorkDomain

data Monad m => FirmyNetDomain m = FirmyNetDomain { dStages :: [WorkStage m], dInitStage :: (WorkStage m -> TVar [WorkerValue] -> m ()) }
instance Monad m => WorkDomainLike (FirmyNetDomain m) m where
  wdName d = UTF8.fromString "FirmyNet"
  wdPort d = 1986
  wdStages d = dStages d
  wdNextStage d s = 
    case UTF8.toString $ wsName s of
	 "GetIndustryLinks" -> head $ filter (\s -> (UTF8.toString $ wsName s) == "GetCompanyLinks") $ dStages d
	 "GetCompanyLinks" -> head $ filter (\s -> (UTF8.toString $ wsName s) == "GetCompanies") $ dStages d
	 "GetCompanies" -> s
  wdInitStage d s = (dInitStage d) s

defaultFirmyNetDomain :: Monad m => FirmyNetDomain m
defaultFirmyNetDomain = FirmyNetDomain {
  dStages= [defaultFirmyGetIndustryLinks,
            defaultFirmyGetCompanyLinks,
	    defaultFirmyGetCompanies],
  dInitStage= undefined
  }

defaultFirmyGetIndustryLinks :: Monad m => WorkStage m
defaultFirmyGetIndustryLinks = WorkStage {
        wsName= UTF8.fromString "GetIndustryLinks"
      , wsWorkJob= undefined
      , wsSaveJobResults= undefined
      }

defaultFirmyGetCompanyLinks :: Monad m => WorkStage m
defaultFirmyGetCompanyLinks = WorkStage {
        wsName= UTF8.fromString "GetCompanyLinks"
      , wsWorkJob= undefined
      , wsSaveJobResults= undefined
      }

defaultFirmyGetCompanies :: Monad m => WorkStage m
defaultFirmyGetCompanies = WorkStage {
        wsName= UTF8.fromString "GetCompanies"
      , wsWorkJob= undefined
      , wsSaveJobResults= undefined
      }