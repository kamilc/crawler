{-# LANGUAGE TypeFamilies, FlexibleInstances, MultiParamTypeClasses #-}
module Contracts.CustomGoogle where

import Data.ByteString.UTF8 as UTF8
import Control.Concurrent.STM.TVar
import WorkDomain

data Monad m => CustomGoogleDomain m = CustomGoogleDomain { dStages :: [WorkStage m], dInitStage :: (WorkStage m -> TVar [WorkerValue] -> m ()) }
instance Monad m => WorkDomainLike (CustomGoogleDomain m) m where
  wdName d = UTF8.fromString "CustomBastards"
  wdPort d = 1907
  wdStages d = dStages d
  wdNextStage d s = head $ dStages d
  wdInitStage d s = (dInitStage d) s

defaultCustomGoogleDomain :: Monad m => CustomGoogleDomain m
defaultCustomGoogleDomain = CustomGoogleDomain {
  dStages= [defaultCustomGoogleGetLinks],
  dInitStage= undefined
  }

defaultCustomGoogleGetLinks :: Monad m => WorkStage m
defaultCustomGoogleGetLinks = WorkStage {
        wsName= UTF8.fromString "GetLinks"
      , wsWorkJob= undefined
      , wsSaveJobResults= undefined
      }