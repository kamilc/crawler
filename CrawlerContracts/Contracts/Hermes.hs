{-# LANGUAGE TypeFamilies, FlexibleInstances, MultiParamTypeClasses #-}
module Contracts.Hermes where

import Data.ByteString.UTF8 as UTF8
import Control.Concurrent.STM.TVar
import WorkDomain

data Monad m => HermesDomain m = HermesDomain { dStages :: [WorkStage m], dInitStage :: (WorkStage m -> TVar [WorkerValue] -> m ()) }
instance Monad m => WorkDomainLike (HermesDomain m) m where
  wdName d = UTF8.fromString "Hermes"
  wdPort d = 1916
  wdStages d = dStages d
  wdNextStage d s = 
    case UTF8.toString $ wsName s of
	 "GetIndustries" -> head $ filter (\s -> (UTF8.toString $ wsName s) == "GetCompanyCards") $ dStages d
	 "GetCompanyCards" -> head $ filter (\s -> (UTF8.toString $ wsName s) == "GetCompanies") $ dStages d
	 "GetCompanies" -> s
  wdInitStage d s = (dInitStage d) s

defaultHermesDomain :: Monad m => HermesDomain m
defaultHermesDomain = HermesDomain {
  dStages= [defaultHermesGetIndustries,
            defaultHermesGetCompanyCards,
            defaultHermesGetCompanies],
  dInitStage= undefined
  }

defaultHermesGetIndustries :: Monad m => WorkStage m
defaultHermesGetIndustries = WorkStage {
        wsName= UTF8.fromString "GetIndustries"
      , wsWorkJob= undefined
      , wsSaveJobResults= undefined
      }

defaultHermesGetCompanyCards :: Monad m => WorkStage m
defaultHermesGetCompanyCards = WorkStage {
        wsName= UTF8.fromString "GetCompanyCards"
      , wsWorkJob= undefined
      , wsSaveJobResults= undefined
      }

defaultHermesGetCompanies :: Monad m => WorkStage m
defaultHermesGetCompanies = WorkStage {
        wsName= UTF8.fromString "GetCompanies"
      , wsWorkJob= undefined
      , wsSaveJobResults= undefined
      }
