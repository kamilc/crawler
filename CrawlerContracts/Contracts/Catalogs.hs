{-# LANGUAGE TypeFamilies, FlexibleInstances, MultiParamTypeClasses #-}
module Contracts.Catalogs where

import Data.ByteString.UTF8 as UTF8
import Control.Concurrent.STM.TVar
import WorkDomain

data Monad m => CatalogsDomain m = CatalogsDomain { dStages :: [WorkStage m], dInitStage :: (WorkStage m -> TVar [WorkerValue] -> m ()) }
instance Monad m => WorkDomainLike (CatalogsDomain m) m where
  wdName d = UTF8.fromString "Catalogs"
  wdPort d = 1926
  wdStages d = dStages d
  wdNextStage d s = s
  wdInitStage d s = (dInitStage d) s

defaultCatalogsDomain :: Monad m => CatalogsDomain m
defaultCatalogsDomain = CatalogsDomain {
  dStages= [defaultCatalogsGetPages, defaultCatalogsWorkPage],
  dInitStage= undefined
  }

defaultCatalogsGetPages :: Monad m => WorkStage m
defaultCatalogsGetPages = WorkStage {
        wsName= UTF8.fromString "GetPages"
      , wsWorkJob= undefined
      , wsSaveJobResults= undefined
      }

defaultCatalogsWorkPage :: Monad m => WorkStage m
defaultCatalogsWorkPage = WorkStage {
        wsName= UTF8.fromString "WorkPage"
      , wsWorkJob= undefined
      , wsSaveJobResults= undefined
      }