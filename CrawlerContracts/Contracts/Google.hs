{-# LANGUAGE TypeFamilies, FlexibleInstances, MultiParamTypeClasses #-}
module Contracts.Google where

import Data.ByteString.UTF8 as UTF8
import Control.Concurrent.STM.TVar
import WorkDomain

data Monad m => GoogleDomain m = GoogleDomain { dStages :: [WorkStage m], dInitStage :: (WorkStage m -> TVar [WorkerValue] -> m ()) }
instance Monad m => WorkDomainLike (GoogleDomain m) m where
  wdName d = UTF8.fromString "Bastards"
  wdPort d = 1906
  wdStages d = dStages d
  wdNextStage d s = head $ dStages d
  wdInitStage d s = (dInitStage d) s

defaultGoogleDomain :: Monad m => GoogleDomain m
defaultGoogleDomain = GoogleDomain {
  dStages= [defaultGoogleGetLinks],
  dInitStage= undefined
  }

defaultGoogleGetLinks :: Monad m => WorkStage m
defaultGoogleGetLinks = WorkStage {
        wsName= UTF8.fromString "GetLinks"
      , wsWorkJob= undefined
      , wsSaveJobResults= undefined
      }