{-# LANGUAGE TypeFamilies, FlexibleInstances, MultiParamTypeClasses #-}
module Contracts.Google.Mails where

import Data.ByteString.UTF8 as UTF8
import Control.Concurrent.STM
import Control.Concurrent.STM.TVar
import WorkDomain

data Monad m => GoogleMailsDomain m = GoogleMailsDomain { dStages :: [WorkStage m], dInitStage :: (WorkStage m -> TVar [WorkerValue] -> m ()) }
instance Monad m => WorkDomainLike (GoogleMailsDomain m) m where
  wdName d = UTF8.fromString "Bastards.Mails"
  wdPort d = 1909
  wdStages d = dStages d
  wdNextStage d s = 
    case UTF8.toString $ wsName s of
   "GetMails" -> s
  wdInitStage d s = (dInitStage d) s

defaultGoogleMailsDomain :: Monad m => GoogleMailsDomain m
defaultGoogleMailsDomain = GoogleMailsDomain {
  dStages= [defaultGoogleGetMails],
  dInitStage= undefined
  }

defaultGoogleGetMails :: Monad m => WorkStage m
defaultGoogleGetMails = WorkStage {
        wsName= UTF8.fromString "GetMails"
      , wsWorkJob= undefined
      , wsSaveJobResults= undefined
      }
