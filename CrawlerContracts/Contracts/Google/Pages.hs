{-# LANGUAGE TypeFamilies, FlexibleInstances, MultiParamTypeClasses #-}
module Contracts.Google.Pages where

import Data.ByteString.UTF8 as UTF8
import Control.Concurrent.STM
import Control.Concurrent.STM.TVar
import WorkDomain

data Monad m => GooglePagesDomain m = GooglePagesDomain { dStages :: [WorkStage m], dInitStage :: (WorkStage m -> TVar [WorkerValue] -> m ()) }
instance Monad m => WorkDomainLike (GooglePagesDomain m) m where
  wdName d = UTF8.fromString "Bastards.Pages"
  wdPort d = 1908
  wdStages d = dStages d
  wdNextStage d s = 
    case UTF8.toString $ wsName s of
      "GetPages" -> s
  wdInitStage d s = (dInitStage d) s

defaultGooglePagesDomain :: Monad m => GooglePagesDomain m
defaultGooglePagesDomain = GooglePagesDomain {
  dStages= [defaultGooglePagesGetPages],
  dInitStage= undefined
  }

defaultGooglePagesGetPages :: Monad m => WorkStage m
defaultGooglePagesGetPages = WorkStage {
        wsName= UTF8.fromString "GetPages"
      , wsWorkJob= undefined
      , wsSaveJobResults= undefined
      }
