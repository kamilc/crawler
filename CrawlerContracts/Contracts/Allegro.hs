{-# LANGUAGE TypeFamilies, FlexibleInstances, MultiParamTypeClasses #-}
module Contracts.Allegro where

import Data.ByteString.UTF8 as UTF8
import Control.Concurrent.STM.TVar
import WorkDomain

data Monad m => AllegroDomain m = AllegroDomain { dStages :: [WorkStage m], dInitStage :: (WorkStage m -> TVar [WorkerValue] -> m ()) }
instance Monad m => WorkDomainLike (AllegroDomain m) m where
  wdName d = UTF8.fromString "Allegro"
  wdPort d = 1929
  wdStages d = dStages d
  wdNextStage d s = s
  wdInitStage d s = (dInitStage d) s

defaultAllegroDomain :: Monad m => AllegroDomain m
defaultAllegroDomain = AllegroDomain {
  dStages= [defaultAllegroGetCategories, defaultAllegroGetSubCategories],
  dInitStage= undefined
  }

defaultAllegroGetCategories :: Monad m => WorkStage m
defaultAllegroGetCategories = WorkStage {
        wsName= UTF8.fromString "GetCategories"
      , wsWorkJob= undefined
      , wsSaveJobResults= undefined
      }

defaultAllegroGetSubCategories :: Monad m => WorkStage m
defaultAllegroGetSubCategories = WorkStage {
        wsName= UTF8.fromString "GetSubCategories"
      , wsWorkJob= undefined
      , wsSaveJobResults= undefined
      }