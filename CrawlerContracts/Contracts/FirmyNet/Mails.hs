{-# LANGUAGE TypeFamilies, FlexibleInstances, MultiParamTypeClasses #-}
module Contracts.FirmyNet.Mails where

import Data.ByteString.UTF8 as UTF8
import Control.Concurrent.STM
import Control.Concurrent.STM.TVar
import WorkDomain

data Monad m => FirmyNetMailsDomain m = FirmyNetMailsDomain { dStages :: [WorkStage m], dInitStage :: (WorkStage m -> TVar [WorkerValue] -> m ()) }
instance Monad m => WorkDomainLike (FirmyNetMailsDomain m) m where
  wdName d = UTF8.fromString "FirmyNet.Mails"
  wdPort d = 1988
  wdStages d = dStages d
  wdNextStage d s = 
    case UTF8.toString $ wsName s of
	 "GetMails" -> s
  wdInitStage d s = (dInitStage d) s

defaultFirmyNetMailsDomain :: Monad m => FirmyNetMailsDomain m
defaultFirmyNetMailsDomain = FirmyNetMailsDomain {
  dStages= [defaultFirmyGetMails],
  dInitStage= undefined
  }

defaultFirmyGetMails :: Monad m => WorkStage m
defaultFirmyGetMails = WorkStage {
        wsName= UTF8.fromString "GetMails"
      , wsWorkJob= undefined
      , wsSaveJobResults= undefined
      }
