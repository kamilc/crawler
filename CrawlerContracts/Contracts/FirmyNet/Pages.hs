{-# LANGUAGE TypeFamilies, FlexibleInstances, MultiParamTypeClasses #-}
module Contracts.FirmyNet.Pages where

import Data.ByteString.UTF8 as UTF8
import Control.Concurrent.STM
import Control.Concurrent.STM.TVar
import WorkDomain

data Monad m => FirmyNetPagesDomain m = FirmyNetPagesDomain { dStages :: [WorkStage m], dInitStage :: (WorkStage m -> TVar [WorkerValue] -> m ()) }
instance Monad m => WorkDomainLike (FirmyNetPagesDomain m) m where
  wdName d = UTF8.fromString "FirmyNet.Pages"
  wdPort d = 1987
  wdStages d = dStages d
  wdNextStage d s = 
    case UTF8.toString $ wsName s of
	 "GetPages" -> s
  wdInitStage d s = (dInitStage d) s

defaultFirmyNetPagesDomain :: Monad m => FirmyNetPagesDomain m
defaultFirmyNetPagesDomain = FirmyNetPagesDomain {
  dStages= [defaultFirmyGetPages],
  dInitStage= undefined
  }

defaultFirmyGetPages :: Monad m => WorkStage m
defaultFirmyGetPages = WorkStage {
        wsName= UTF8.fromString "GetPages"
      , wsWorkJob= undefined
      , wsSaveJobResults= undefined
      }
