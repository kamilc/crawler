{-# LANGUAGE TypeFamilies, MultiParamTypeClasses, FlexibleInstances, FunctionalDependencies #-}
module WorkDomain where

import Data.ByteString.UTF8 as UTF8
import Data.ByteString.Lazy.UTF8 as LUTF8
import Data.UUID
import Data.MessagePack
import Data.Binary.Put
import Data.Attoparsec
import Data.Maybe (fromMaybe)
import Control.Concurrent.STM.TVar

type WorkerId = UTF8.ByteString -- UUID
-- instance Packable UUID where
--   put uuid = putLazyByteString $ toByteString uuid
--   
-- instance Unpackable UUID where
--   get = do
--     dane <- takeLazyByteString
--     return $ fromMaybe nil $ fromByteString dane
--     
-- instance OBJECT UUID

type WorkStageId = UTF8.ByteString
type DomainId = UTF8.ByteString

type WorkerValue = UTF8.ByteString

data Monad m => WorkStage m = WorkStage {
    wsName :: WorkStageId
  , wsWorkJob :: (WorkerValue -> m [WorkerValue])
  , wsSaveJobResults :: (WorkerId -> WorkerValue -> [WorkerValue] -> m ())
  , wsCountDone :: m Integer
  , wsCountAll :: m Integer
}

class Monad m => WorkDomainLike d m | d -> m where
  wdName :: d -> DomainId
  wdPort :: d -> Int
  wdStages :: d -> [WorkStage m]
  wdNextStage :: d -> WorkStage m -> WorkStage m
  wdInitStage :: d -> WorkStage m -> TVar [WorkerValue] -> m ()