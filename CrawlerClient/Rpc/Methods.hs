module Rpc.Methods where

import Network.MessagePackRpc.Client
import CrawlerContracts

nextJob :: RpcMethod (WorkerId -> IO (Maybe WorkerValue))
nextJob = method "nextJob"

returnJob :: RpcMethod (WorkerId -> [WorkerValue] -> IO ())
returnJob = method "returnJob"

currentStageName :: RpcMethod (IO WorkerValue)
currentStageName = method "currentStageName"