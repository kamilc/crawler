module Restarter ( runRestarter
                 , safelyRestartNetwork 
                 , isConnected
                 ) where

import CrawlerClient
import Control.Concurrent
import Control.Concurrent.STM
import Control.Monad.Reader
import System.Process
import System.Exit
import Text.Regex.PCRE
import System.Log.Logger
import qualified Data.Map as Map

runRestarter :: CrawlerAction ()
runRestarter = do
  app <- ask
  liftIO $ putStrLn $ "Kolejne restartowanie sieci zaplanowane w przeciągu " ++ (show $ clientRestartDelay app) ++ " sekund"
  liftIO $ infoM "Crawler.Restarter.runRestarter" $ "Kolejne restartowanie sieci zaplanowane w przeciągu " ++ (show $ clientRestartDelay app) ++ " sekund"
  liftIO $ threadDelay $ 1000000 * (clientRestartDelay app)
  safelyRestartNetwork
  runRestarter

safelyRestartNetwork :: CrawlerAction ()
safelyRestartNetwork = do
  liftIO $ putStrLn "Bezpiecznie restartuję internet."
  liftIO $ infoM "Crawler.Restarter" "Bezpiecznie restartuję internet"
  disallowDownloading
  waitForDownloadersToStop
  restartComputerNetworking
  clearServerConnections
  allowDownloading

-- HELPERS ---------------------------------
clearServerConnections :: CrawlerAction ()
clearServerConnections = do
  tconns <- return .clientServerConnections =<< ask
  liftIO $ atomically $ do
    writeTVar tconns $ Map.empty

restartComputerNetworking :: CrawlerAction ()
restartComputerNetworking = do
  isconnected <- isConnected
  connName <- getConnectionName
  case isconnected of
    True -> do
      liftIO $ putStrLn "Wykryto aktywne połączenie z internetem - zamykanie przed ponownym startem."
      liftIO $ infoM "Crawler.Restarter" "Wykryto aktywne połączenie z internetem - zamykanie przed ponownym startem."
      stopNetworking connName
      liftIO $ threadDelay $ 5000000
      startNetworking connName
      liftIO $ threadDelay $ 5000000
    False -> do
      liftIO $ putStrLn "Wykryto  brak aktywnego połączenie z internetem - ponowny start."
      liftIO $ infoM "Crawler.Restarter" "Wykryto  brak aktywnego połączenie z internetem - ponowny start."
      startNetworking connName
      liftIO $ threadDelay $ 5000000
  nowconnected <- isConnected
  when (not nowconnected) restartComputerNetworking

getConnectionName :: CrawlerAction String
getConnectionName = do
  conn <- liftIO $ readProcess "nmcli" ["-t", "-f", "NAME", "con", "status"] [] --nmcli -t -f NAME con status
  liftIO $ debugM "Crawler.Restarter.getConnectionName" $ "Wykryto nazwane połączenie: " ++ conn
  case init conn of
    "" -> do
      restartWholeNetwork
      getConnectionName
    _ -> return $ init conn
    
restartWholeNetwork :: CrawlerAction ()
restartWholeNetwork = do
  liftIO $ debugM "NetAccess.restartWholeNetwork" $ "Wykryto brak nazwanego połączenia - restartuje całą sieć."
  result <- liftIO $ system "nmcli nm enable true"
  liftIO $ threadDelay $ 5000000
  case result of
    ExitSuccess -> return ()
    ExitFailure _ -> do
      liftIO $ threadDelay $ 5000000
      restartWholeNetwork

isConnected :: CrawlerAction Bool
isConnected = do
  conStateString <- liftIO $ readProcess "nmcli" ["dev"] []
  liftIO $ debugM "Crawler.Restarter.isConnected" $ "Odebrano od manadżera połączeń sieciowych informacje o połączeniach:\n" ++ conStateString
  return $ (conStateString =~ (".*połączono.*" :: String) :: Bool)

stopNetworking :: String -> CrawlerAction ()
stopNetworking name = do
  liftIO $ debugM "Crawler.Restarter.stopNetworking" $ "Uruchamiam komendę menedżera sieciowego w trybie CLI: " ++ ("nmcli -t con down id \"" ++ name ++ "\"")
  result <- liftIO $ system $ "nmcli -t con down id \"" ++ name ++ "\"" --"nmcli nm enable false"
  liftIO $ threadDelay $ 10000000
  case result of
    ExitSuccess -> do
      liftIO $ debugM "Crawler.Restarter.stopNetworking" $ "Komenda " ++ ("nmcli -t con down id \"" ++ name ++ "\"") ++ " zwróciła status ExitSuccess"
      return ()
    ExitFailure _ -> do
      liftIO $ debugM "Crawler.Restarter.stopNetworking" $ "Komenda " ++ ("nmcli -t con down id \"" ++ name ++ "\"") ++ " zwróciła status ExitFailure"
      liftIO $ threadDelay $ 1000000
      liftIO $ putStrLn "Ponownie próbuję wyłączyć połączenie internetowe.."
      stopNetworking name

startNetworking :: String -> CrawlerAction ()
startNetworking name = do 
  liftIO $ debugM "Crawler.Restarter.startNetworking" $ "Uruchamiam komendę menedżera sieciowego w trybie CLI: " ++ ("nmcli -t con up id \"" ++ name ++ "\"")
  result <- liftIO $ system $ "nmcli -t con up id \"" ++ name ++ "\"" --"nmcli nm enable true"
  liftIO $ threadDelay $ 10000000
  case result of
    ExitSuccess -> do 
      liftIO $ debugM "Crawler.Restarter.startNetworking" $ "Komenda " ++ ("nmcli -t con up id \"" ++ name ++ "\"") ++ " zwróciła status ExitSuccess"
      return ()
    ExitFailure _ -> do
      liftIO $ debugM "Crawler.Restarter.startNetworking" $ "Komenda " ++ ("nmcli -t con up id \"" ++ name ++ "\"") ++ " zwróciła status ExitFailure"
      liftIO $ threadDelay $ 1000000
      liftIO $ putStrLn "Ponownie próbuję włączyć połączenie internetowe.."
      startNetworking name

disallowDownloading :: CrawlerAction ()
disallowDownloading = setDownloading False

allowDownloading :: CrawlerAction ()
allowDownloading = setDownloading True

setDownloading :: Bool -> CrawlerAction ()
setDownloading setup = do
  app <- ask
  liftIO $ debugM "Crawler.Restarter.setDownloading" $ "Ustawianie statusu pozwalania na używanie sieci na " ++ (show setup)
  liftIO $ atomically $ writeTVar (clientAllowDownloads app) setup

waitForDownloadersToStop :: CrawlerAction ()
waitForDownloadersToStop = do
  progress <- liftIO . atomically . readTVar =<< return . clientJobsInProgress =<< ask
  when (not $ null progress) waitAndTryAgain
  where
    waitAndTryAgain = do
      liftIO $ debugM "Crawler.Restarter.waitForDownloadersToStop" "Czekam na inne wątki aby dokończyły rozpoczęte ściągania."
      liftIO $ threadDelay $ 1000000
      waitForDownloadersToStop
