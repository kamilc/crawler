{-# LANGUAGE OverloadedStrings, FlexibleContexts #-}
module Main where
import System
import System.Console.GetOpt
import Control.Exception
import Control.Monad.Reader
import CrawlerClient
import Control.Concurrent
import Control.Concurrent.STM
import CrawlerContracts
import Domains
import Random
import Data.UUID
import Worker
import Restarter
import Rpc.Methods
import Data.List.Split
import List (elemIndex)
import qualified Data.ByteString.UTF8 as UTF8

-- Argumenty -----------------------------------
options :: [OptDescr (Options -> IO Options)]
options = [
    Option ['V'] ["version"] (NoArg showVersion)         "pokaż numer wersji",
    Option ['s'] ["server"]   (OptArg serverAddress "ADDRESS")   "adres serwera crawlerd",
    Option ['r'] ["restart-delay"]  (OptArg restartDelay "DELAY") "ilość sekund co którą restart sieci. -1 dla wyłączenia restartowania",
    Option ['d'] ["domains"]  (ReqArg chosenDomains "DOMAINS") "oddzielana przecinkami lista domen do wystartowania",
    Option ['t'] ["threads"]  (ReqArg chosenThreads "THREADS") "liczba wątków"
  ]

showVersion :: Options -> IO Options
showVersion _ = do
  putStrLn "crawler 0.3"
  exitWith ExitSuccess

chosenThreads :: String -> Options -> IO Options
chosenThreads arg opt = return opt { optThreads= (read arg) }

chosenDomains :: String -> Options -> IO Options
chosenDomains arg opt = do
  let domains = splitOn "," arg
  case Prelude.null domains of
    False -> do
      return $ opt { optDomains= domains }
    True -> do
      putStrLn "Musisz wybrać przynajmniej jedną domenę zadań oddzielając je przecinkami."
      exitFailure

serverAddress :: Maybe String -> Options -> IO Options
serverAddress marg opt = 
  case marg of
    Nothing -> return opt
    Just arg -> return $ opt { optServer= arg }

restartDelay :: Maybe String -> Options -> IO Options
restartDelay marg opt = 
  case marg of
    Nothing -> return opt
    Just arg -> do
      edelay <- try $ return $ read arg
      case edelay of
        Left (SomeException _) -> do
          putStrLn "Wartość 'restart-delay' musi być liczbą całkowitą."
          exitFailure
        Right delay -> return $ opt { optRestartDelay= delay }

header :: String
header = "Uzycie: crawler [OPCJE...]"

-- Main ------------------------------------------
main :: IO ()
main = do
  args <- getArgs
  case getOpt RequireOrder options args of
    (actions, [],      [])     -> runMain actions
    (_,     nonOpts, [])     -> error $ "Nierozpoznane argumenty: " ++ unwords nonOpts
    (_,     _,       msgs)   -> do error $ concat msgs ++ usageInfo header options
  where
    runMain actions = do
      opts <- foldl (>>=) (return defaultOptions) actions
      runReaderT runCrawlerClient =<< defaultCrawlerClient opts

runCrawlerClient :: CrawlerAction ()
runCrawlerClient = do
  app <- ask
  runDomainAsync =<< getGoogleDomain
  runDomainAsync =<< getGooglePagesDomain
  runDomainAsync =<< getGoogleMailsDomain
  runDomainAsync =<< getCustomGoogleDomain
  runDomainAsync =<< getFirmyNetDomain
  runDomainAsync =<< getFirmyNetPagesDomain
  runDomainAsync =<< getFirmyNetMailsDomain
  runDomainAsync =<< getHermesDomain
  runDomainAsync =<< getCatalogsDomain
  when (clientRestartDelay app > 0) runRestarter
  waitForCtrlC
  where
    waitForCtrlC = do
      liftIO $ threadDelay $ 1000000
      waitForCtrlC
    runDomainAsync domain = do
      app <- ask
      _ <- liftIO $ forkIO $ runReaderT (runDomainRpc domain) app
      return ()

runDomainRpc :: WorkDomainLike d CrawlerAction => d -> CrawlerAction ()
runDomainRpc d = do
  app <- ask
  case elemIndex (UTF8.toString $ wdName d) (clientDomains app) of
    Just _ -> do
      tfirststagename <- liftIO $ atomically $ newTVar ""
      let domena' = DomainContext app tfirststagename d
      conn <- liftIO $ getDomainServerConnection d app
      firststagename <- liftIO $ currentStageName conn
      liftIO $ putStrLn $ "Domena \"" ++ (UTF8.toString $ wdName d) ++ "\" otrzymała informacje o aktualnej fazie: \"" 
                                      ++ (UTF8.toString firststagename) ++ "\""
      liftIO $ atomically $ writeTVar tfirststagename firststagename
      let domena = domena' { ctxStageName= tfirststagename }
      liftIO $ mapM_ (\_ -> forkIO $ runReaderT runDomainWorker domena) $ ( [1..(clientThreads app)] :: [Int] )
    Nothing -> return ()

runDomainWorker :: WorkDomainLike d CrawlerAction => ReaderT (DomainContext d) IO ()
runDomainWorker = do
  app <- return . ctxApp =<< ask
  domain <- return .ctxDomain =<< ask
  stagename <- return . ctxStageName =<< ask
  uuid <- return . UTF8.fromString =<< return . toString =<< liftIO (randomIO :: IO UUID)
  liftIO $ runReaderT runWorker $ Worker uuid app domain stagename
