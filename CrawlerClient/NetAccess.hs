{-# LANGUAGE DoAndIfThenElse, OverloadedStrings, FlexibleContexts #-}
module NetAccess where

import Prelude hiding (catch)
import CrawlerClient
import Control.Concurrent.STM.TVar
import Control.Concurrent.STM
import Control.Monad.Reader
import Network.URI
import Network.Browser
import Network.HTTP hiding (Connection)
import Codec.Binary.UTF8.String
import qualified Data.ByteString.Lazy as BL
import qualified Codec.Compression.GZip as GZip
import Control.Concurrent
import Control.Exception hiding (finally, bracket, catch)
import System.Log.Logger
import qualified Data.ByteString.UTF8 as UTF8
import CrawlerContracts
import Data.List (delete)
import Control.Applicative
import Restarter
import System.Timeout

safelyDownloadHtml :: WorkerValue -> CrawlerAction (Maybe UTF8.ByteString)
safelyDownloadHtml url = do
  mhtml <- safelyBrowse url $ downloadUrl url
  case mhtml of
    Left (a,_,c) -> do
      case ((a == 4)) of
        True -> return Nothing
        False -> do
          liftIO $ threadDelay $ 1000000
          safelyDownloadHtml url
    Right html -> return $ Just $ snd $ html

safelyStatefulDownloadHtml :: WorkerValue -> Maybe (BrowserState (HandleStream BL.ByteString)) -> CrawlerAction (Maybe (BrowserState (HandleStream BL.ByteString), UTF8.ByteString))
safelyStatefulDownloadHtml url mstate = do
  mhtml <- safelyBrowse url $ downloadStatefulUrl url mstate
  case mhtml of
    Left (a,_,c) -> do
      case ((a == 4)) of
        True -> return Nothing
        False -> do
          liftIO $ threadDelay $ 1000000
          safelyStatefulDownloadHtml url mstate
    Right html -> return $ Just (fst html, snd html)

safelyBrowseW :: WorkDomainLike d CrawlerAction => WorkerValue -> NetAction a -> WorkerAction d a
safelyBrowseW job action = do
  tallow <- return . clientAllowDownloads =<< return . workerApp =<< ask
  tprogress <- return . clientJobsInProgress =<< return . workerApp =<< ask
  allow <- liftIO $ atomically $ readTVar tallow
  case allow of
       False -> do
         liftIO $ threadDelay 100000
         safelyBrowseW job action
       True -> do
         liftIO $ atomically $ do
           progress <- readTVar tprogress
           writeTVar tprogress $ (job:progress)
         eresult <- liftIO $ try (runReaderT action $ NetContext tallow tprogress)
         liftIO $ atomically $ do
           progress <- readTVar tprogress
           writeTVar tprogress $ delete job progress
         case eresult of
            Left (SomeException e) -> do
              liftIO $ threadDelay $ 1000000
              liftIO $ putStrLn $ "Bezpiecznie przechwytuję nieoczekiwany wyjątek i próbuję ponownie."
              liftIO $ putStrLn $ "Treść wyjątku - " ++ (show e) ++ "\n"
              safelyBrowseW job action
            Right result -> return result

safelyBrowse :: WorkerValue -> NetAction a -> CrawlerAction a
safelyBrowse job action = do
  tallow <- return . clientAllowDownloads =<< ask
  tprogress <- return . clientJobsInProgress =<< ask
  allow <- liftIO $ atomically $ readTVar tallow
  case allow of
     False -> do
       liftIO $ threadDelay 1000000
       safelyBrowse job action
     True -> do
       connected <- isConnected
       case connected of
         False -> do
           liftIO $ putStrLn "Brak połączenia internetowego gdy się go spodziewałem."
           liftIO $ putStrLn "Startuję internet wcześniej niż planowano."
           safelyRestartNetwork
           safelyBrowse job action
         True -> do
           liftIO $ atomically $ do
             progress <- readTVar tprogress
             writeTVar tprogress $ (job:progress)
           eresult <- liftIO $ try (runReaderT action $ NetContext tallow tprogress)
           liftIO $ atomically $ do
             progress <- readTVar tprogress
             writeTVar tprogress $ delete job progress
           case eresult of
              Left (SomeException e) -> do
                liftIO $ putStrLn $ "Bezpiecznie przechwytuję nieoczekiwany wyjątek i próbuję ponownie za 10s. ( zadanie: " ++ (UTF8.toString job) ++ ")"
                liftIO $ putStrLn $ "Treść wyjątku - " ++ (show e) ++ "\n"
                liftIO $ threadDelay $ 10000000
                safelyBrowse job action
              Right result -> return result

toUri :: String -> URI
toUri url = case parseURI $ (escapeURIString isUnescapedInURI . encodeString) url of
              Nothing -> error $ "Błąd programowy podczas tworzenie adresu URL -" ++ url
              Just uri -> uri

downloadUrl :: UTF8.ByteString -> NetAction (Either (Int,Int,Int) (BrowserState (HandleStream BL.ByteString), UTF8.ByteString))
downloadUrl url = do
  ret <- liftIO $ timeout (10*10^6) $ browse $ do
          (_, rsp) <- do
            ioAction $ putStrLn $ "downloadUrl " ++ (UTF8.toString url)
            setAllowRedirects True
            setOutHandler $ const (return ())
            setUserAgent "Mozilla/5.0 (X11; Linux x86_64; rv:5.0) Gecko/20100101 Firefox/5.0"
            request $ createRequest uri
          case rspCode rsp of
            (2,_,_) -> do
              state <- getBrowserState
              let body = rspBody rsp
              if isGz rsp then return $ Right (state, UTF8.fromString $ decode $ BL.unpack $ GZip.decompress body)
      	      else             return $ Right (state, UTF8.fromString $ decode $ BL.unpack $ body)
            _ -> do
      	    ioAction $ warningM "Crawler.NetAccess.downloadUrl" $ "Otrzymałem kod " ++ (show $ rspCode rsp) ++ " dla połączenia z adresem " ++ (show uri)
      	    return $ Left $ rspCode rsp
  case ret of
    Nothing -> do
      liftIO $ putStrLn $ "Request dla " ++ (UTF8.toString url) ++ " nie dostał odpowiedzi w przeciągu 10 sekund. Próbuję jeszcze raz."
      downloadUrl url
    Just a -> return a
  where
    isGz rsp = maybe False (== "gzip") $ findHeader HdrContentEncoding rsp
    uri = toUri $ UTF8.toString url
    
downloadStatefulUrl :: UTF8.ByteString -> Maybe (BrowserState (HandleStream BL.ByteString)) -> NetAction (Either (Int,Int,Int) (BrowserState (HandleStream BL.ByteString), UTF8.ByteString))
downloadStatefulUrl url mstate = do
  case mstate of
    Nothing -> downloadUrl url
    Just _state -> do
      ret <- liftIO $ timeout (10*10^6) $ browse $ (withBrowserState _state) $ do
              (_, rsp) <- do
                ioAction $ putStrLn $ "downloadStatefulUrl " ++ (UTF8.toString url)
                --cookies <- getCookies
                --setCookies $ map (\c -> c {ckComment= Just "expires=Sun, 26-Feb-2012 09:17:52 GMT;"} ) cookies
                --ioAction $ putStrLn $ show cookies
                setAllowRedirects True
                --setOutHandler putStrLn
                setUserAgent "Mozilla/5.0 (X11; Linux x86_64; rv:5.0) Gecko/20100101 Firefox/5.0"
                request $ createRequest uri
              case rspCode rsp of
                (2,_,_) -> do
                  state <- getBrowserState
                  let a = rspHeaders rsp
                  ioAction $ putStrLn $ show a -- hdrValue (a!!5)
                  let body = rspBody rsp
                  if isGz rsp then return $ Right (state, UTF8.fromString $ decode $ BL.unpack $ GZip.decompress body)
      	          else             return $ Right (state, UTF8.fromString $ decode $ BL.unpack $ body)
                _ -> do
      	        ioAction $ warningM "Crawler.NetAccess.downloadUrl" $ "Otrzymałem kod " ++ (show $ rspCode rsp) ++ " dla połączenia z adresem " ++ (show uri)
      	        return $ Left $ rspCode rsp
      case ret of
        Nothing -> do
          liftIO $ putStrLn $ "Request dla " ++ (UTF8.toString url) ++ " nie dostał odpowiedzi w przeciągu 10 sekund. Próbuję jeszcze raz."
          downloadUrl url
        Just a -> return a
  where
    isGz rsp = maybe False (== "gzip") $ findHeader HdrContentEncoding rsp
    uri = toUri $ UTF8.toString url

createRequest :: URI -> Request BL.ByteString
createRequest uri = replaceHeader HdrHost "www.google.pl" $  replaceHeader HdrAccept "application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5" $ replaceHeader HdrUserAgent "Mozilla/5.0 (X11; Linux x86_64; rv:5.0) Gecko/20100101 Firefox/5.0" $ replaceHeader HdrAcceptLanguage "pl-PL,pl;q=0.8,en-US;q=0.6,en;q=0.4" $ replaceHeader HdrConnection "keep-alive" $ replaceHeader HdrContentEncoding "gzip, deflate" $ replaceHeader HdrAcceptCharset "utf-8" $ defaultGETRequest_ $ uri
