module Paths_CrawlerClient (
    version,
    getBinDir, getLibDir, getDataDir, getLibexecDir,
    getDataFileName
  ) where

import Data.Version (Version(..))
import System.Environment (getEnv)

version :: Version
version = Version {versionBranch = [0,2], versionTags = []}

bindir, libdir, datadir, libexecdir :: FilePath

bindir     = "/usr/local/bin"
libdir     = "/usr/local/lib/CrawlerClient-0.2/ghc-7.0.3"
datadir    = "/usr/local/share/CrawlerClient-0.2"
libexecdir = "/usr/local/libexec"

getBinDir, getLibDir, getDataDir, getLibexecDir :: IO FilePath
getBinDir = catch (getEnv "CrawlerClient_bindir") (\_ -> return bindir)
getLibDir = catch (getEnv "CrawlerClient_libdir") (\_ -> return libdir)
getDataDir = catch (getEnv "CrawlerClient_datadir") (\_ -> return datadir)
getLibexecDir = catch (getEnv "CrawlerClient_libexecdir") (\_ -> return libexecdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
