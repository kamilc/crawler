{-# LANGUAGE DoAndIfThenElse, OverloadedStrings, FlexibleContexts #-}
module Worker where
import CrawlerClient
import CrawlerContracts
import Control.Monad.Reader
import Control.Concurrent.STM.TVar
import Control.Concurrent
import Control.Concurrent.STM
import Control.Exception
import Rpc.Methods
import NetAccess
import qualified Data.ByteString.UTF8 as UTF8

runWorker :: WorkDomainLike d CrawlerAction =>  WorkerAction d ()
runWorker = do
  safelyUpdateStageInfo
  safelyReturnJob =<< safelyWorkJob =<< safelyGetJob
  runWorker

safelyUpdateStageInfo :: WorkDomainLike d CrawlerAction =>  WorkerAction d ()
safelyUpdateStageInfo = do
  cid <- return . workerId =<< ask
  app <- return . workerApp =<< ask
  domain <- return . workerDomain =<< ask
  stageName <- safelyBrowseW cid $ do
    conn <- liftIO $ getDomainServerConnection domain app
    liftIO $ currentStageName conn
  liftIO $ putStrLn $ "Otrzymałem nazwę fazy: " ++ UTF8.toString stageName
  tStageName <- return . workerStageName =<< ask
  liftIO $ atomically $ do
    writeTVar tStageName stageName

safelyGetJob :: WorkDomainLike d CrawlerAction =>  WorkerAction d (Maybe WorkerValue)
safelyGetJob = do
  cid <- return . workerId =<< ask
  app <- return . workerApp =<< ask
  domain <- return . workerDomain =<< ask
  mword <- safelyBrowseW cid $ do
    conn <- liftIO $ getDomainServerConnection domain app
    liftIO $ nextJob conn cid
  case mword of
    Nothing -> do
      liftIO $ threadDelay $ 10000000
      return Nothing
    Just word -> do
      case UTF8.length word == 0 of
        True -> do
          liftIO $ threadDelay $ 10000000
          return Nothing
        False -> do
          worker <- ask
          liftIO $ putStrLn $ "Pobrano zadanie: \"" ++ (UTF8.toString word) ++ "\" dla domeny: " ++ (show $ wdName $ workerDomain worker)
          return $ Just word

safelyWorkJob :: WorkDomainLike d CrawlerAction =>  Maybe WorkerValue -> WorkerAction d [WorkerValue]
safelyWorkJob mjob = do
  case mjob of
       Nothing -> return []
       Just job -> do
         stage <- currentStage
         app <- return . workerApp =<< ask
         eret <- liftIO ( try (runReaderT ((wsWorkJob stage) job) app) :: IO (Either SomeException [WorkerValue]) )
         case eret of
           Left err -> do
             liftIO $ putStrLn $ "Bezpiecznie przechwytuje wyjątek i próbuję ponownie: " ++ (show err)
             liftIO $ threadDelay 100000
             safelyWorkJob mjob
           Right ret -> return ret
  where
    currentStage = do
      stages <- return . wdStages =<< return . workerDomain =<< ask
      name <- liftIO . atomically . readTVar =<< return . workerStageName =<< ask
      return $ head $ filter (\s -> wsName s == name) stages

safelyReturnJob :: WorkDomainLike d CrawlerAction => [WorkerValue] -> WorkerAction d ()
safelyReturnJob links = 
  case links of
    [] -> return ()
    _  -> do
      cid <- return . workerId =<< ask
      domena <- return . workerDomain =<< ask
      app <- return . workerApp =<< ask
      safelyBrowseW cid $ do
        conn <- liftIO $ getDomainServerConnection domena app
        liftIO $ returnJob conn cid links
      liftIO $ putStrLn $ "Domena: " ++ (show $ wdName domena) ++ " -> Zwróciłem wyniki zadania (" ++ (show $ length links) ++ ") wyników"
