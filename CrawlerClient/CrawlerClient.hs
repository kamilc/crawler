{-# LANGUAGE OverloadedStrings, FlexibleContexts #-}

module CrawlerClient where

import Control.Concurrent.STM.TVar
import Control.Monad.Reader
import Network.MessagePackRpc.Client
import qualified Data.Map as Map
import Control.Concurrent.STM

import CrawlerContracts

data WorkDomainLike d CrawlerAction => DomainContext d = DomainContext { 
  ctxApp :: CrawlerClient,
  ctxStageName :: TVar WorkStageId,
  ctxDomain :: d
}

data WorkDomainLike d CrawlerAction => Worker d = Worker {
  workerId :: WorkerId,
  workerApp :: CrawlerClient,
  workerDomain :: d,
  workerStageName :: TVar WorkStageId
}

data CrawlerClient = CrawlerClient { 
  clientServer  :: String,
  clientWorkersNumber  :: Int,
  clientRestartDelay   :: Int,
  clientAllowDownloads :: TVar Bool,
  clientServerConnectionAllowance :: TVar Bool,
  clientServerConnections :: TVar (Map.Map DomainId Connection),
  clientJobsInProgress :: TVar [WorkerValue],
  clientDomains :: [String],
  clientThreads :: Int
}

data NetContext = NetContext {
  netAllowDownloads :: TVar Bool,
  netJobsInProgress :: TVar [WorkerValue]
  }

type NetAction = ReaderT NetContext IO

defaultCrawlerClient :: Options -> IO CrawlerClient
defaultCrawlerClient opt = do
  tallow <- tAllowDownloads
  tallowance <- tServerConnectionAllowance
  tconns <- tServerConnections
  tprogress <- tProgress
  return CrawlerClient {
    clientServer= optServer opt,
    clientWorkersNumber= 8,
    clientRestartDelay= optRestartDelay opt,
    clientAllowDownloads= tallow,
    clientServerConnectionAllowance= tallowance,
    clientServerConnections= tconns,
    clientJobsInProgress= tprogress,
    clientDomains= optDomains opt,
    clientThreads= optThreads opt
    }
  where
    tAllowDownloads = atomically $ newTVar True
    tServerConnectionAllowance = atomically $ newTVar True
    tServerConnections = atomically $ newTVar Map.empty
    tProgress = atomically $ newTVar []

getDomainServerConnection :: WorkDomainLike d CrawlerAction => d -> CrawlerClient -> IO Connection
getDomainServerConnection domain app = do
  conns <- connections
  case Map.lookup (wdName domain) conns of
       Nothing -> do
         putStrLn $ "Inicjuję połączenie na " ++ host ++ ":" ++ (show port)
         connection <- connect host port
         let newConns = Map.insert (wdName domain) connection conns
         atomically $ writeTVar (clientServerConnections app) newConns
         return connection
       Just connection -> return connection
  where 
    connections = atomically $ readTVar $ clientServerConnections app
    host = clientServer app
    port = wdPort domain
  

data Options = Options {
  optServer :: String,    -- Adres IP serwera 'crawlerd'
  optRestartDelay :: Int,  -- Ilość sekund co którą wątek restartera rozpocznie procedurę restartu
  optDomains :: [String],
  optThreads :: Int
  }

defaultOptions :: Options
defaultOptions = Options {
  optServer= "localhost",
  optRestartDelay= 150,
  optDomains= [],
  optThreads= 8
  }

type CrawlerAction = ReaderT CrawlerClient IO
type WorkerAction d = ReaderT (Worker d) IO
