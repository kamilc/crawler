module Domains.CustomGoogle where

import CrawlerContracts
import CrawlerClient
import Contracts.CustomGoogle
import Control.Concurrent
import Control.Concurrent.STM.TVar
import NetAccess
import Control.Monad.Reader
import Text.Printf
import Text.Regex.PCRE
import qualified Data.ByteString.UTF8 as UTF8
import qualified Data.ByteString as B

getCustomGoogleDomain :: CrawlerAction (CustomGoogleDomain CrawlerAction)
getCustomGoogleDomain = return $ ((CustomGoogleDomain  [(getCustomGoogleStage :: WorkStage CrawlerAction )] googleCustomInit) )

getCustomGoogleStage :: WorkStage CrawlerAction
getCustomGoogleStage = defaultCustomGoogleGetLinks {
  wsWorkJob= googleCustomWorkJob
  }

googleCustomWorkJob :: WorkerValue -> CrawlerAction [WorkerValue]
googleCustomWorkJob word = do
  return . parseCustomGoogle =<< return . B.concat =<< mapM (\i -> safelyDownloadCustomGoogle word (10*i) 10) [0..9]
  where
    safelyDownloadCustomGoogle _word start num = do
      mhtml <- downloadCustomGoogle _word start num
      case mhtml of
          Nothing -> do
            liftIO $ threadDelay $ 1000000
            safelyDownloadCustomGoogle _word start num
          Just html -> return html

googleCustomInit :: WorkStage CrawlerAction -> TVar [WorkerValue] -> CrawlerAction ()
googleCustomInit _ _ = return ()

-- PARSER ----------------------------------------------------------------
parseCustomGoogle :: UTF8.ByteString -> [UTF8.ByteString]
parseCustomGoogle html = 
  concat (html =~ ("(?<=<h3 class=\"r\"><a href=\")[^\"]*" :: String) :: [[UTF8.ByteString]])

-- NET -------------------------------------------------------------------
downloadCustomGoogle :: UTF8.ByteString -> Int -> Int -> CrawlerAction (Maybe UTF8.ByteString)
downloadCustomGoogle word start limit = do
  safelyDownloadHtml url
  where
    url = UTF8.fromString $ "http://www.google.pl/search?ie=UTF-8&q=" ++ (UTF8.toString word) 
      ++ "&num=" ++ (printf "%d" limit) ++"&start=" ++ (printf "%d" start)