{-# LANGUAGE OverloadedStrings #-}
module Domains.Hermes where

import CrawlerContracts
import CrawlerClient
import Contracts.Hermes
import Control.Concurrent
import Control.Concurrent.STM.TVar
import NetAccess
import Control.Monad.Reader
import Text.Parsec.Prim
import Text.Parsec.Combinator
import Text.Parsec.Char
import Text.Parsec.ByteString
import Text.Parsec.Error
import Codec.Binary.UTF8.String
import qualified Data.ByteString.UTF8 as UTF8
import Data.String.Utils

getHermesDomain :: CrawlerAction (HermesDomain CrawlerAction)
getHermesDomain = return $ ((HermesDomain  [(getIndustriesStage :: WorkStage CrawlerAction ),
                                            (getCardsStage      :: WorkStage CrawlerAction ),
                                            (getCompaniesStage  :: WorkStage CrawlerAction )] hermesInit) )

getIndustriesStage :: WorkStage CrawlerAction
getIndustriesStage = defaultHermesGetIndustries {
  wsWorkJob= hermesIndustriesWorkJob
  }

getCardsStage :: WorkStage CrawlerAction
getCardsStage = defaultHermesGetCompanyCards {
  wsWorkJob= hermesCardsWorkJob
  }
  
getCompaniesStage :: WorkStage CrawlerAction
getCompaniesStage = defaultHermesGetCompanies {
  wsWorkJob= hermesCompaniesWorkJob
  }

hermesIndustriesWorkJob :: WorkerValue -> CrawlerAction [WorkerValue]
hermesIndustriesWorkJob url = do
  case (UTF8.toString url) == "www.pkt.pl" of
    True -> return [""]
    False -> do
      mhtml <- safelyDownloadHtml url
      case mhtml of
        Nothing -> return [""]
        Just html -> do
          case parseHermesIndustries html of
            Left err -> do
              liftIO $ putStrLn $ show err
              return [""]
            Right industries -> do
              liftIO $ mapM_ (\i -> liftIO $ putStrLn $ UTF8.toString $ i) industries
              case null industries of
                False -> 
                  case parseNextIndustryUrl html of
                    Nothing -> return industries
                    Just nextUrl -> do
                      moreOfThem <- hermesIndustriesWorkJob nextUrl
                      return $ industries ++ moreOfThem
                True -> return [""]
          
hermesCardsWorkJob :: WorkerValue -> CrawlerAction [WorkerValue]
hermesCardsWorkJob url = do
  mhtml <- safelyDownloadHtml url
  case mhtml of
    Nothing -> return [""]
    Just html -> do
      case parseHermesCompanies html of
        Left _ -> do
          liftIO $ threadDelay 100000
          return [""]
        Right companies -> do
          liftIO $ mapM_ (\i -> liftIO $ putStrLn $ UTF8.toString $ i) companies
          case parseNextCompaniesUrl html of
            Nothing -> return companies
            Just nextUrl -> do
              moreOfThem <- hermesCardsWorkJob nextUrl
              return $ companies ++ moreOfThem

hermesCompaniesWorkJob :: WorkerValue -> CrawlerAction [WorkerValue]
hermesCompaniesWorkJob url = do
  mhtml <- safelyDownloadHtml url
  case mhtml of
    Nothing -> return []
    Just html -> do
      case parseHermes html of
        Left _ -> do
          liftIO $ threadDelay 100000
          hermesCompaniesWorkJob url
        Right company -> do
          --liftIO $ mapM_ (\i -> liftIO $ putStrLn $ UTF8.toString $ i) company
          return company

hermesInit :: WorkStage CrawlerAction -> TVar [WorkerValue] -> CrawlerAction ()
hermesInit _ _ = return ()

-- PARSER ----------------------------------------------------------------
p_nextUrl :: Parser [UTF8.ByteString]
p_nextUrl = do
  mark <- _skipToNext' (string "http://i.wp.pl/a/i/wiadomosci_04/prawo.gif") ""
  case null mark of
    True -> return [] 
    False -> do
      _ <- _skipToNext (string "href=\"")
      href <- many $ noneOf "\""
      return [UTF8.fromString $ replace "&amp;" "&" href]

parseNextCompaniesUrl :: UTF8.ByteString -> Maybe UTF8.ByteString
parseNextCompaniesUrl html = 
  case parse p_nextUrl [] html of
    Left err -> Just $ UTF8.fromString $ show err
    Right nextUrls -> 
      case null nextUrls of
        True -> Nothing
        False -> Just $ UTF8.fromString $ ("http://firmy.wp.pl" ++  (UTF8.toString $ head nextUrls))

parseNextIndustryUrl :: UTF8.ByteString -> Maybe UTF8.ByteString
parseNextIndustryUrl html = 
  case parse p_nextUrl [] html of
    Left err -> Just $ UTF8.fromString $ show err
    Right nextUrls -> 
      case null nextUrls of
        True -> Nothing
        False ->  Just $ UTF8.fromString $ ("http://firmy.wp.pl/branze/" ++  (UTF8.toString $ (head nextUrls)))

parseHermesIndustries :: UTF8.ByteString -> Either ParseError [UTF8.ByteString]
parseHermesIndustries html = parse p_industries [] html
  where
    p_industries :: Parser [UTF8.ByteString]
    p_industries = do
      _ <- _skipToNext (string "szFWyniki")
      _ <- _skipToNext (string "<table style=\"width: 100%;\"><tr>")
      manyTill p_industry (try (string "<br /></td><td></td></tr></table>") <|> try (string "<td></td></tr></table>"))
    p_industry :: Parser UTF8.ByteString
    p_industry = do
      _ <- try (string "<br /></td><td>") <|> try (string "<td>") <|> string "<br />"
      _ <- _skipToNext' (string "<a href=\"") ""
      href <- many $ noneOf "\""
      _ <- many $ noneOf "/"
      _ <- count 3 anyChar
      return $ UTF8.fromString ("http://firmy.wp.pl" ++ href)

parseHermesCompanies :: UTF8.ByteString -> Either ParseError [UTF8.ByteString]
parseHermesCompanies html = do
  parse p_companies [] html
  where
    p_companies :: Parser [UTF8.ByteString]
    p_companies = do
      mark <- _skipToNext' (string "javascript:WP.stat.eventDot('oYpDet-3'); okno('") ""
      case null mark of
        True -> return []
        False -> do
          href <- many $ noneOf "'"
          rest <- p_companies
          return ((UTF8.fromString href):rest)

parseHermes :: UTF8.ByteString -> Either ParseError [UTF8.ByteString]
parseHermes html = 
  parse p_companyInfo [] html
  where
    p_companyInfo :: Parser [UTF8.ByteString]
    p_companyInfo = do
      _ <- _skipToNext $ string "class=\"fn"
      _ <- anyChar >> anyChar
      name <- many $ noneOf "<"
      postal <- _skipToNext' (p_data "postal-code" "<") ""
      city <- _skipToNext' (p_data "locality" "<,") ""
      street <- _skipToNext' (p_data "street-address" "<") ""
      district <- _skipToNext' (p_data "extended-location\">woj" ",") ""
      phones <- _skipToNext' (p_data "phones" "<") ""
      setInput html
      mail <- _skipToNext' (p_mail) ""
      setInput html
      www <- _skipToNext' (p_www) ""
      return $ map (UTF8.fromString . decodeString . UTF8.toString) [(UTF8.fromString name), postal, 
                                                                      city, street, district, phones, 
                                                                      www, mail]
    p_data ::  String -> String -> Parser UTF8.ByteString
    p_data mark end = do
      _mark <- _skipToNext' (string $ mark) ""
      case null _mark of
        True -> return ""
        False -> do
          _ <- anyChar >> anyChar
          ret <- many $ noneOf end
          return $ UTF8.fromString ret
    p_www :: Parser UTF8.ByteString
    p_www = do
      mark <- _skipToNext' (string "repname_website") ""
      case null mark of
        True -> return ""
        False -> do
          _ <- _skipToNext $ string ">"
          ret <- many $ noneOf "<"
          return $ UTF8.fromString ret
    p_mail :: Parser UTF8.ByteString
    p_mail = do
      mark <- _skipToNext' (string "repname_email ") ""
      case null mark of
        True -> return ""
        False -> do
          _ <- _skipToNext $ string ">"
          ret <- many $ noneOf "<"
          return $ UTF8.fromString ret
      

_skipToNext :: Parser a -> Parser a
_skipToNext parser = do
  res <- optionMaybe $ try parser
  case res of
    Nothing -> do
      anyChar >> _skipToNext parser
    Just r -> return r

_skipToNext' :: Parser a -> a -> Parser a
_skipToNext' parser fallback = do
  maybeEof <- optionMaybe $ try eof
  case maybeEof of
    Just _ -> return fallback
    Nothing -> do
      res <- optionMaybe $ try parser
      case res of
        Just r -> return r
        Nothing -> do
          anyChar >> _skipToNext' parser fallback
