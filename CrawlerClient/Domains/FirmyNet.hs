{-# LANGUAGE OverloadedStrings #-}
module Domains.FirmyNet where

import CrawlerClient
import CrawlerContracts
import Contracts.FirmyNet
import Control.Concurrent.STM.TVar
import NetAccess
import qualified Data.ByteString.UTF8 as UTF8
import Text.Parsec.Prim
import Text.Parsec.Combinator
import Text.Parsec.Char
import Text.Parsec.ByteString
import Text.Parsec.Error
import Control.Concurrent
import Control.Monad.Reader
import Codec.Binary.UTF8.String

-- Domena ---------------------------------------------------
getFirmyNetDomain :: CrawlerAction (FirmyNetDomain CrawlerAction)
getFirmyNetDomain = return $ ((FirmyNetDomain  [(getFirmyIndustryLinksStage :: WorkStage CrawlerAction ),
                                                (getFirmyCompanyLinksStage  :: WorkStage CrawlerAction ),
                                                (getFirmyCompaniesStage     :: WorkStage CrawlerAction )] firmyInit))

getFirmyIndustryLinksStage :: WorkStage CrawlerAction
getFirmyIndustryLinksStage = defaultFirmyGetIndustryLinks {
  wsWorkJob= firmyWorkIndustryLinks
}

getFirmyCompanyLinksStage :: WorkStage CrawlerAction
getFirmyCompanyLinksStage = defaultFirmyGetCompanyLinks {
  wsWorkJob= firmyWorkCompanyLinks
}

getFirmyCompaniesStage :: WorkStage CrawlerAction
getFirmyCompaniesStage = defaultFirmyGetCompanies {
  wsWorkJob= firmyWorkCompanies
}

firmyInit :: WorkStage CrawlerAction -> TVar [WorkerValue] -> CrawlerAction ()
firmyInit _ _ = return ()

firmyWorkIndustryLinks :: WorkerValue -> CrawlerAction [WorkerValue]
firmyWorkIndustryLinks root = do
  mhtml <- safelyDownloadHtml root
  case mhtml of
    Nothing -> return []
    Just html -> do
      case parseIndustries html of
        Left _ -> do
          liftIO $ threadDelay 1000000
          firmyWorkIndustryLinks root
        Right industries -> do
          return industries

firmyWorkCompanyLinks :: WorkerValue -> CrawlerAction [WorkerValue]
firmyWorkCompanyLinks url = do
  mhtml <- safelyDownloadHtml url
  case mhtml of
    Nothing -> return []
    Just html -> do
      case parseCompanies html of
        Left _ -> do
          liftIO $ threadDelay 1000000
          firmyWorkCompanyLinks url
        Right companies -> do
          return companies

firmyWorkCompanies :: WorkerValue -> CrawlerAction [WorkerValue]
firmyWorkCompanies url = do
  mhtml <- safelyDownloadHtml url
  case mhtml of
    Nothing -> return [""]
    Just html -> do
      case parseCompanyInfo html of
        Left err -> do
          liftIO $ putStrLn $ show err
          return [""]
        Right infos -> do
          --liftIO $ mapM_ (\i -> liftIO $ putStrLn $ UTF8.toString $ i) infos
          return infos

-- Net -------------------------------------------------------


-- Parser ----------------------------------------------------
parseCompanyInfo :: UTF8.ByteString -> Either ParseError [UTF8.ByteString]
parseCompanyInfo html = parse p_companyInfo [] html
  where
    p_companyInfo :: Parser [UTF8.ByteString]
    p_companyInfo = do
      _ <- skipToNext $ string "http://schema.org/LocalBusiness"
      name <- skipToNext $ p_companyName
      _ <- skipToNext $ string "http://schema.org/PostalAddress"
      postalCode <- skipToNext $ p_companyPostalCode
      city <-  skipToNext $ p_companyCity
      street <-  skipToNext $ p_companyStreet
      district <-  skipToNext $ p_companyDistrict
      phone <-  skipToNext $ p_companyPhone
      www <-  skipToNext $ p_companyWWW
      return $ map (UTF8.fromString . decodeString . UTF8.toString) [name, postalCode, city, street, district, phone, www]
    p_companyName :: Parser UTF8.ByteString
    p_companyName = do
      skipToNext' (p_itemProp "name") ""
    p_companyPostalCode :: Parser UTF8.ByteString
    p_companyPostalCode = do
      skipToNext' (p_itemProp "postalCode") ""
    p_companyCity :: Parser UTF8.ByteString
    p_companyCity = do
      skipToNext' (p_itemProp "addressLocality") ""
    p_companyStreet :: Parser UTF8.ByteString
    p_companyStreet = do
      skipToNext' (p_itemProp "streetAddress") ""
    p_companyDistrict :: Parser UTF8.ByteString
    p_companyDistrict = do
      skipToNext' (p_itemProp "addressRegion") ""
    p_companyPhone :: Parser UTF8.ByteString
    p_companyPhone = do
      skipToNext' (p_itemProp "telephone") ""
    p_companyWWW :: Parser UTF8.ByteString
    p_companyWWW = do
      mark <- skipToNext' (string "<a") ""
      case null mark of
         True -> return ""
         False -> do
           mblank <- lookAhead $ optionMaybe $ try p_BlankBeforeClose
           case mblank of
             Nothing -> p_companyWWW
             Just _ -> do
               _ <- skipToNext $ string "href=\""
               href <- many $ noneOf "\""
               return $ UTF8.fromString href
    p_BlankBeforeClose :: Parser UTF8.ByteString
    p_BlankBeforeClose = do
      mret <- optionMaybe $ try $ string "rel=\"blank\""
      case mret of
        Nothing -> do
          noneOf ">" >> p_BlankBeforeClose
        Just ret -> return $ UTF8.fromString ret
    p_itemProp :: String -> Parser UTF8.ByteString
    p_itemProp prop = do
      mark <- skipToNext' (string $ "itemprop=\"" ++ prop ++ "\"") ""
      case null mark of
        True -> return ""
        False -> do
          _ <- many $ noneOf ">"
          _ <- anyChar
          ret <- many $ noneOf "<"
          return $ UTF8.fromString ret

parseCompanies :: UTF8.ByteString -> Either ParseError [UTF8.ByteString]
parseCompanies html = parse p_Companies [] html
 where
   p_Companies :: Parser [UTF8.ByteString]
   p_Companies = do
     manyTill (skipToNext p_company) (try eof)
   p_company :: Parser UTF8.ByteString
   p_company = do
     ret <- between (string "<h2>") (string "</h2>") p_company_h2
     _ <- skipToNext $ string "comlist" <|> string "</html>"
     return ret
   p_company_h2 :: Parser UTF8.ByteString
   p_company_h2 = do
     _ <- skipToNext $ string "href=\""
     href <- many $ noneOf "\""
     _ <- skipToNext $ string "</a>"
     return $ UTF8.fromString href

parseIndustries :: UTF8.ByteString -> Either ParseError [UTF8.ByteString]
parseIndustries html = parse p_Industries [] html
  where
    p_Industries :: Parser [UTF8.ByteString]
    p_Industries = do
      _ <- skipToNext $ string "side nicelinks"
      industries <- manyTill (skipToNext p_industry) (try (string "</ul>"))
      return $ map (UTF8.fromString . show) industries
    p_industry :: Parser (UTF8.ByteString, Int)
    p_industry = do
      between (string "<li>") (string "</li>") p_industry_li
      where
        p_industry_li = do
          _ <- string "<a href=\""
          href <- many $ noneOf "\""
          _ <- skipToNext $ string "<span>"
          _ <- skipToNext $ string "</a>"
          _ <- many space
          number <- between (char '(') (char ')') $ many1 digit
          return (UTF8.fromString href, read number)

skipToNext :: Parser a -> Parser a
skipToNext parser = do
  res <- optionMaybe $ try parser
  case res of
    Nothing -> do
      anyChar >> skipToNext parser
    Just r -> return r

skipToNext' :: Parser a -> a -> Parser a
skipToNext' parser fallback = do
  maybeEof <- optionMaybe $ try eof
  case maybeEof of
    Just _ -> return fallback
    Nothing -> do
      res <- optionMaybe $ try parser
      case res of
        Just r -> return r
        Nothing -> do
          anyChar >> skipToNext' parser fallback

