{-# LANGUAGE OverloadedStrings #-}
module Domains.Google.Pages where

import CrawlerContracts
import CrawlerClient
import Contracts.Google.Pages
import Control.Concurrent.STM
import Control.Monad.Reader
import Data.List (find)
import Text.Parsec.Prim
import Text.Parsec.Combinator
import Text.Parsec.Char
import Text.Parsec.ByteString
import NetAccess
import qualified Data.ByteString.UTF8 as UTF8
import Network.URL
import Data.Maybe
import Data.Char

-- Domena ---------------------------------------------------
getGooglePagesDomain :: CrawlerAction (GooglePagesDomain CrawlerAction)
getGooglePagesDomain = return $ ((GooglePagesDomain  [(getGooglePagesStage :: WorkStage CrawlerAction )] googlePagesInit))

getGooglePagesStage :: WorkStage CrawlerAction
getGooglePagesStage = defaultGooglePagesGetPages {
  wsWorkJob= googlePagesWorkJob
}

googlePagesWorkJob :: WorkerValue -> CrawlerAction [WorkerValue]
googlePagesWorkJob url = do
  mhtml <- safelyDownloadHtml url
  case mhtml of
    Nothing -> return [""]
    Just html -> return [html]

googlePagesInit :: WorkStage CrawlerAction -> TVar [WorkerValue] -> CrawlerAction ()
googlePagesInit _ _ = return ()