{-# LANGUAGE OverloadedStrings #-}
module Domains.FirmyNet.Mails where

import CrawlerContracts
import CrawlerClient
import Contracts.FirmyNet.Mails
import Control.Concurrent.STM
import Control.Monad.Reader
import Text.Parsec.Prim
import Text.Parsec.Combinator
import Text.Parsec.Char
import Text.Parsec.ByteString
import Text.Parsec.Error
import qualified Data.ByteString.UTF8 as UTF8
import Data.List (nub)

-- Domena ---------------------------------------------------
getFirmyNetMailsDomain :: CrawlerAction (FirmyNetMailsDomain CrawlerAction)
getFirmyNetMailsDomain = return $ ((FirmyNetMailsDomain  [(getFirmyMailsStage :: WorkStage CrawlerAction )] firmyMailsInit))

getFirmyMailsStage :: WorkStage CrawlerAction
getFirmyMailsStage = defaultFirmyGetMails {
  wsWorkJob= firmyMailsWorkJob
}

firmyMailsWorkJob :: WorkerValue -> CrawlerAction [WorkerValue]
firmyMailsWorkJob html =
  case parseHtmlMails html of
    Left _ -> do
      return [""]
    Right mails -> do
      liftIO $ mapM_ (\i -> liftIO $ putStrLn $ UTF8.toString $ i) $ nub mails
      return $ nub mails

firmyMailsInit :: WorkStage CrawlerAction -> TVar [WorkerValue] -> CrawlerAction ()
firmyMailsInit _ _ = return ()

-- Parsec ----------------------------------------------------------------------
parseHtmlMails :: UTF8.ByteString -> Either ParseError [UTF8.ByteString]
parseHtmlMails = parse p_getMails []
  where
    p_getMails :: Parser [UTF8.ByteString]
    p_getMails = do
      mark <- _trySkipToNext p_mail ""
      case null $ UTF8.toString mark of
        True -> return []
        False -> do
          rest <- p_getMails
          return (mark:rest)
    p_mail :: Parser UTF8.ByteString
    p_mail = do
      beg <- letter
      resBeg <- many (try letter <|> oneOf ".-_")
      at <- char '@'
      ending <- many (try letter <|> oneOf "-_")
      domains <- many1 p_domain
      return $ UTF8.fromString ([beg] ++ resBeg ++ [at] ++ ending ++ (concat domains))
    p_domain :: Parser String
    p_domain = do
      dot <- char '.'
      ending <- many (try letter <|> oneOf "-_")
      return (dot:ending)

_trySkipToNext :: Parser a -> a -> Parser a
_trySkipToNext parser fallback = do
  maybeEof <- optionMaybe $ try eof
  case maybeEof of
    Just _ -> return fallback
    Nothing -> do
      res <- optionMaybe $ try parser
      case res of
        Just r -> return r
        Nothing -> do
          anyChar >> _trySkipToNext parser fallback