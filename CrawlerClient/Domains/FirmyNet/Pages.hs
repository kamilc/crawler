{-# LANGUAGE OverloadedStrings #-}
module Domains.FirmyNet.Pages where

import CrawlerContracts
import CrawlerClient
import Contracts.FirmyNet.Pages
import Control.Concurrent.STM
import Control.Monad.Reader
import Data.List (find)
import Text.Parsec.Prim
import Text.Parsec.Combinator
import Text.Parsec.Char
import Text.Parsec.ByteString
import NetAccess
import qualified Data.ByteString.UTF8 as UTF8
import Network.URL
import Data.Maybe
import Data.Char
import Text.Regex

-- Domena ---------------------------------------------------
getFirmyNetPagesDomain :: CrawlerAction (FirmyNetPagesDomain CrawlerAction)
getFirmyNetPagesDomain = return $ ((FirmyNetPagesDomain  [(getFirmyPagesStage :: WorkStage CrawlerAction )] firmyPagesInit))

getFirmyPagesStage :: WorkStage CrawlerAction
getFirmyPagesStage = defaultFirmyGetPages {
  wsWorkJob= firmyPagesWorkJob
}

firmyPagesWorkJob :: WorkerValue -> CrawlerAction [WorkerValue]
firmyPagesWorkJob url = do
  case (take 2 $ reverse $ UTF8.toString url) == "/." of
    True -> do
      tdone <- liftIO $ atomically $ newTVar []
      firmyWorkPageTree tdone $ UTF8.fromString $ take ((length $ UTF8.toString url) - 2) $ UTF8.toString url
    False -> do
      tdone <- liftIO $ atomically $ newTVar []
      firmyWorkPageTree tdone url

firmyWorkPageTree :: TVar [WorkerValue] -> WorkerValue -> CrawlerAction [WorkerValue]
firmyWorkPageTree tdone url = do
  done <- liftIO $ atomically $ readTVar tdone
  case find (\d -> d == url) done of
    Just _ -> return []
    Nothing -> do
      mhtml <- safelyDownloadHtml url
      liftIO $ atomically $ do
        writeTVar tdone (url:done)
      case mhtml of
        Nothing -> return []
        Just html -> do
          -- wyciagamy wszystkie linki z tej domeny
          links <- filterOutDone =<< return . filterOutNonLocal =<< return . normalizeLinks =<< return . filterOutInvalid =<< parsePageLinks html
          -- zwracamy w liscie ten html oraz htmle dla tych linkow
          moreHtmls <- return . concat =<< mapM (firmyWorkPageTree tdone) links
          return (html:moreHtmls)
  where
    parsePageLinks :: UTF8.ByteString -> CrawlerAction [UTF8.ByteString]
    parsePageLinks html = do
      let ampRegex = mkRegex "&amp;"
      let _html = UTF8.fromString $ subRegex ampRegex (UTF8.toString html) "&"
      case parse p_pageLinks [] _html of
        Left err -> do
          liftIO $ putStrLn $ "Błąd podczas wyciągania linków ze strony. Komunikat: " ++ (show err)
          return []
        Right links -> return links
      where
        p_pageLinks :: Parser [UTF8.ByteString]
        p_pageLinks = do
          mark <- trySkipToNext (try (string "href=\"") <|> string "href='") ""
          case null mark of
            True -> return []
            False -> do
              link <- many $ noneOf "\"'"
              rest <- p_pageLinks
              return ((UTF8.fromString link):rest)
    
    filterOutInvalid :: [UTF8.ByteString] -> [UTF8.ByteString]
    filterOutInvalid = 
      map UTF8.fromString
      . map (exportURL . fromJust) 
      . filter isJust 
      . map importURL
      . map UTF8.toString
      . filter (\l -> "mailto:" /= (take 7 $ UTF8.toString l))
      . filter (\l -> ".." /= (take 2 $ UTF8.toString l))
      . filter (\l -> "javascript:" /= (take 11 $ UTF8.toString l))
      . filter onlyPages
      
    onlyPages :: UTF8.ByteString -> Bool
    onlyPages = not . inExcludeList . (take 4) . reverse . UTF8.toString
      where
        inExcludeList :: String -> Bool
        inExcludeList ending = 
          case find (\excluded -> (map toLower ending) == (reverse excluded) ) excludeList of
            Nothing -> False
            Just _  -> True
        excludeList :: [String]
        excludeList = [".jpg", ".jpeg", ".gif", ".png", ".bmp", ".pdf", 
                       ".doc", ".docx", ".pps", ".xls", ".avi", ".swf",
                       ".mp3", ".mp4", ".mpeg", ".css", ".js", ".svg",
                       ".ttf", ".odf", ".tiff"]
            
    
    normalizeLinks :: [UTF8.ByteString] -> [UTF8.ByteString]
    normalizeLinks = map normalizeLink
    
    normalizeLink :: UTF8.ByteString -> UTF8.ByteString
    normalizeLink link =
      case url_type $ fromJust $ importURL $ UTF8.toString link of
        Absolute _    -> link
        HostRelative  -> UTF8.fromString $ "http://" ++ (urlHost $ UTF8.toString url) ++ 
                         (if (null $ UTF8.toString link) || ((head $ UTF8.toString link) == '/') then "" else "/") ++ UTF8.toString link
        PathRelative  -> UTF8.fromString $ "http://" ++  (urlHost $ UTF8.toString url) ++ 
                         (if (null $ UTF8.toString link) || ((head $ UTF8.toString link) == '/') then "" else "/") ++ UTF8.toString link
    
    filterOutNonLocal :: [UTF8.ByteString] -> [UTF8.ByteString]
    filterOutNonLocal = filter (\l -> (urlHost $ UTF8.toString l) == (urlHost $ UTF8.toString url) )
    
    filterOutDone :: [UTF8.ByteString] -> CrawlerAction [UTF8.ByteString]
    filterOutDone links = do
      done <- liftIO $ atomically $ readTVar tdone
      return $ filter (\l -> isNothing $ find (\d -> d == l) done) links
    
    urlHost :: String -> String
    urlHost _url =
     case url_type $ fromJust $ importURL _url of
       Absolute h -> host h
       _ -> ""

firmyPagesInit :: WorkStage CrawlerAction -> TVar [WorkerValue] -> CrawlerAction ()
firmyPagesInit _ _ = return ()

-- Parsec ----------------------------------------------------------------------
trySkipToNext :: Parser a -> a -> Parser a
trySkipToNext parser fallback = do
  maybeEof <- optionMaybe $ try eof
  case maybeEof of
    Just _ -> return fallback
    Nothing -> do
      res <- optionMaybe $ try parser
      case res of
        Just r -> return r
        Nothing -> do
          anyChar >> trySkipToNext parser fallback


