module Domains.Google where

import CrawlerContracts
import CrawlerClient
import Contracts.Google
import Control.Concurrent
import Control.Concurrent.STM.TVar
import NetAccess
import Control.Monad.Reader
import Text.Printf
import Text.Regex.PCRE
import qualified Data.ByteString.UTF8 as UTF8
import qualified Data.ByteString as B

getGoogleDomain :: CrawlerAction (GoogleDomain CrawlerAction)
getGoogleDomain = return $ ((GoogleDomain  [(getGoogleStage :: WorkStage CrawlerAction )] googleInit) )

getGoogleStage :: WorkStage CrawlerAction
getGoogleStage = defaultGoogleGetLinks {
  wsWorkJob= googleWorkJob
  }

googleWorkJob :: WorkerValue -> CrawlerAction [WorkerValue]
googleWorkJob word = do
  return . parseGoogle =<< return . B.concat =<< mapM (\i -> safelyDownloadGoogle word (10*i) 10) [0..9]
  where
    safelyDownloadGoogle _word start num = do
      mhtml <- downloadGoogle _word start num
      case mhtml of
          Nothing -> do
            liftIO $ threadDelay $ 1000000
            safelyDownloadGoogle _word start num
          Just html -> return html

googleInit :: WorkStage CrawlerAction -> TVar [WorkerValue] -> CrawlerAction ()
googleInit _ _ = return ()

-- PARSER ----------------------------------------------------------------
parseGoogle :: UTF8.ByteString -> [UTF8.ByteString]
parseGoogle html = 
  concat (html =~ ("(?<=<h3 class=\"r\"><a href=\")[^\"]*" :: String) :: [[UTF8.ByteString]])

-- NET -------------------------------------------------------------------
downloadGoogle :: UTF8.ByteString -> Int -> Int -> CrawlerAction (Maybe UTF8.ByteString)
downloadGoogle word start limit = do
  safelyDownloadHtml url
  where
    url = UTF8.fromString $ "http://www.google.pl/search?ie=UTF-8&q=" ++ (UTF8.toString word) 
      ++ "&num=" ++ (printf "%d" limit) ++"&start=" ++ (printf "%d" start)