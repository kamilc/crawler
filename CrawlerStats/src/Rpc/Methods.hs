module Rpc.Methods where

import Network.MessagePackRpc.Client

rpcServerVersion :: RpcMethod (IO String)
rpcServerVersion = method "serverVersion"

rpcServerDomainNames :: RpcMethod (IO [String])
rpcServerDomainNames = method "serverDomainNames"

rpcServerDomainInfo :: RpcMethod (String -> IO [String])
rpcServerDomainInfo = method "serverDomainInfo"