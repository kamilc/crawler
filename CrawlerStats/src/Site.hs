{-# LANGUAGE OverloadedStrings #-}

{-|

This is where all the routes and handlers are defined for your site. The
'site' function combines everything together and is exported by this module.

-}

module Site
  ( site
  ) where

import           Control.Applicative
import           Snap.Extension.Heist
import           Snap.Extension.Timer
import           Snap.Util.FileServe
import           Snap.Types
import           Text.Templating.Heist
import           Text.JSON
import qualified Data.Text as T
import           CrawlerServer
import           Control.Monad.IO.Class
import           Data.Maybe (fromMaybe)
import qualified Data.ByteString.UTF8 as UTF8
import Text.Show

import           Application


------------------------------------------------------------------------------
-- | Renders the front page of the sample site.
--
-- The 'ifTop' is required to limit this to the top of a route.
-- Otherwise, the way the route table is currently set up, this action
-- would be given every request.
index :: Application ()
index = ifTop $ heistLocal (bindSplices indexSplices) $ render "index"
  where
    indexSplices =
        [ ("start-time",   startTimeSplice)
        , ("current-time", currentTimeSplice)
        ]

serverConnect :: Application ()
serverConnect = do
  address <- decodedParam "serverAddress"
  modifyResponse $ setContentType "application/json"
  runs <- liftIO $ serverRuns address
  case runs of
    False -> do
      writeText $ T.pack $ encode $ toJSObject [("serverStatus", JSBool False)]
    True -> do
      version <- liftIO $ serverVersion address
      let object = toJSObject  [("serverStatus",  JSBool     True),
                                ("serverVersion", JSString $ toJSString version)]
      writeText $ T.pack $ encode object

serverDomainInfos :: Application ()
serverDomainInfos = do
  address <- decodedParam "serverAddress"
  modifyResponse $ setContentType "application/json"
  domainNames <- liftIO $ serverDomainNames address
  domainsInfos <- liftIO $ mapM (serverDomainInfo address) domainNames
  let objects = map (\domain -> JSObject $ toJSObject [("name",         JSString $ toJSString $ domain !! 0),
                                            ("currentPhase", JSObject $ toJSObject [
                                                      ("name", JSString $ toJSString $ domain !! 1),
                                                      ("countDone", JSRational False $ fromIntegral ((read $ domain !! 2) :: Int)),
                                                      ("countAll", JSRational False $ fromIntegral ((read $ domain !! 3) :: Int)),
                                                      ("timeToEnd", JSRational False $ fromIntegral ((read $ domain !! 4) :: Int))])]) domainsInfos
  let object = JSArray objects
  writeText $ T.pack $ (showJSArray $ objects) "" -- $ encode object

------------------------------------------------------------------------------
decodedParam p = return . UTF8.toString =<< fromMaybe "" <$> getParam p

------------------------------------------------------------------------------
-- | The main entry point handler.
site :: Application ()
site = route [ ("/",            index),
               ("/server/connect", serverConnect),
               ("/server/domains", serverDomainInfos)
             ]
       <|> serveDirectory "resources/static"
