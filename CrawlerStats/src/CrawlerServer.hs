module CrawlerServer where

import Network.MessagePackRpc.Client
import Rpc.Methods
import Control.Exception

serverRuns :: String -> IO Bool
serverRuns address = do
  econn <- _tryConnect address
  case econn of
    Left _ -> return False
    Right _ -> return True

serverVersion :: String -> IO String
serverVersion address = do
  econn <- _tryConnect address
  case econn of
    Left _ -> return ""
    Right conn -> do
      rpcServerVersion conn

serverDomainNames :: String -> IO [String]
serverDomainNames address = do
  econn <- _tryConnect address
  case econn of
    Left _ -> return []
    Right conn -> do
      rpcServerDomainNames conn

serverDomainInfo :: String -> String -> IO [String]
serverDomainInfo address name = do
  econn <- _tryConnect address
  case econn of
    Left _ -> return []
    Right conn -> do
      rpcServerDomainInfo conn name

_tryConnect :: String -> IO (Either SomeException Connection)
_tryConnect address = try $ connect address 1905