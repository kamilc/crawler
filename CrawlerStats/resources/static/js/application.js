$.fn.spin = function(opts) {
  this.each(function() {
    var $this = $(this),
        data = $this.data();

    if (data.spinner) {
      data.spinner.stop();
      delete data.spinner;
    }
    if (opts !== false) {
      data.spinner = new Spinner($.extend({color: $this.css('color')}, opts)).spin(this);
    }
  });
  return this;
};

$(function(){
	$('#tabs').tabs();
	$('#home-accordion').accordion({ header: "h3", autoHeight: false });
	$('#settings-accordion').accordion({ header: "h3", autoHeight: false });
	$( "input:submit" ).button();

	// knockout:

	var viewModel = {
      serverAddress: ko.observable('localhost'),
      serverStatus: ko.observable('Brak połączenia'),
      serverVersion: ko.observable('Brak połączenia'),
      serverDomains: ko.observable('Brak połączenia'),
      serverDomainsArray: ko.observable([]),
      serverIsConnecting: ko.observable(false),

      tryConnectServer: function(){
      	viewModel.serverIsConnecting(true);
      	$.ajax({
    		  url: "/server/connect?serverAddress=" + viewModel.serverAddress(),
    		  dataType: 'json',
    		  error: function(){
    		  	viewModel.serverIsConnecting(false);
    		  },
    		  success: function(serverData) {
    		    // tutaj serwer zwraca JSONa z zawartością:
    		    // serverStatus : Bool
    		    // serverVersion: String
    		    // serverDomains: [{name: String, countDone: Integer, countAll: Integer, timeToEnd: Integer}]
    		    if(serverData.serverStatus){
    		    	viewModel.serverStatus("Serwer jest uruchomiony");
    		    	viewModel.serverVersion(serverData.serverVersion);
    		    	viewModel.serverIsConnecting(false);
              viewModel.tryFetchDomainsInfo();
    		    }
            else {
              viewModel.serverIsConnecting(false);
              viewModel.serverStatus("Serwer nie jest uruchomiony");
            }
    		  }
    		});
      },

      tryFetchDomainsInfo: function(){
        $.ajax({
          url: "/server/domains?serverAddress=" + viewModel.serverAddress(),
          dataType: 'json',
          success: function(serverData) {
            viewModel.serverDomainsArray($.map(serverData, function(item){
                return {
                  name: item.name,
                  currentPhase: {name: item.currentPhase.name, countDone: item.currentPhase.countDone, 
                                 countAll: item.currentPhase.countAll, timeToEnd: item.currentPhase.timeToEnd}
                };
              }));
          }
        });
      },

      tryRefreshServer: function(){
        
      },

      allowHomeDomainsTabs: function(){
        $(document).oneTime(500, function(){
          $('#home-accordion-domains').accordion({ header: "h3", autoHeight: false });
        });
      }
    }
    ko.applyBindings(viewModel);

    ko.bindingHandlers.spinVisible = {
    	update: function(element, valueAccessor, allBindingsAccessor) {
          if(valueAccessor())
          	$(element).spin();
          else
            $(element).stop();
        }
    };
});