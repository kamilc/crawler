<div id="home-accordion">
  <div>
    <h3><a href="#">Status serwera</a></h3>
    <div>
      <center>
        <table>
          <tr>
            <td>Adres serwera crawlerd:</td>
            <td class="listing-value" data-bind="text: serverAddress"></td>
          </tr>
          <tr>
            <td>Status serwera crawlerd:</td>
            <td class="listing-value"><span class="ui-icon ui-icon-alert tab-icon" data-bind="visible: serverStatus() == 'Brak połączenia'"></span><span  data-bind="text: serverStatus, style: { color: serverStatus() == 'Brak połączenia' ? '' : (serverStatus() == 'Serwer nie jest uruchomiony' ? 'red': 'green') }"></span></td>
          </tr>
          <tr>
            <td>Wersja serwera crawlerd:</td>
            <td class="listing-value"><span class="ui-icon ui-icon-alert tab-icon" data-bind="visible: serverVersion().length == 0"></span><span  data-bind="text: serverVersion"></span></td>
          </tr>
          <tr>
            <td>Działające na serwerze domeny pracy:</td>
            <td class="listing-value"><span class="ui-icon ui-icon-alert tab-icon" data-bind="visible: serverDomains().length == 0"></span><span  data-bind="text: serverDomains"></span></td>
          </tr>
        </table>
      </center>
    </div>
  </div>
</div>

<div id="home-accordion-domains" data-bind='template: { name: "domainStatsTemplate", foreach: serverDomainsArray, afterRender: allowHomeDomainsTabs }'></div>

<!-- Template'y -->
<script id='domainStatsTemplate' type='text/html'>
  <div>
    <h3><a href="#">${$data.name}</a></h3>
    <div>
      <center>
        <table width="100%">
          <tr>
            <td>Aktualna faza:</td>
            <td class="listing-value" data-bind="text: $data.currentPhase.name"></td>
          </tr>
          <tr>
            <td>Wykonano:</td>
            <td class="listing-value">
              <span data-bind="text: $data.currentPhase.countDone"></span> 
              ze wszystkich:
              <span data-bind="text: $data.currentPhase.countAll"></span> 
            </td>
          </tr>
          <tr>
            <td>Postęp aktualnej fazy:</td>
            <td class="listing-value"><span data-bind="text: (100*($data.currentPhase.countDone/$data.currentPhase.countAll))"></span>%</td>
          </tr>
          <tr>
            <td>Spodziewane ukończenie prac fazy:</td>
            <td class="listing-value"><span data-bind="text: $data.currentPhase.timeToEnd"></span> godzin.</td>
          </tr>
        </table>
      </center>
    </div>
  </div>
</script>