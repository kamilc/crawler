<html>
  <head>
    <title>Monitor prac systemu Crawler</title>
    <link rel="stylesheet" type="text/css" href="screen.css"/>
    <link type="text/css" href="css/smoothness/jquery-ui-1.8.16.custom.css" rel="stylesheet" /> 
    <script type="text/javascript" src="js/jquery-1.6.2.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="js/jquery.tmpl.js"></script>
    <script type="text/javascript" src="js/jquery.timers.js"></script>
    <script type="text/javascript" src="js/knockout.js"></script>
    <script type="text/javascript" src="js/spin.js"></script>
    <script type="text/javascript" src="js/application.js"></script>
  </head>
  <body>
    <div id="content">
      <div id="tabs">
        <ul>
          <li><a href="#tabs-home"><span class="ui-icon ui-icon-home tab-icon"></span> Statystyki</a></li>
          <li><a href="#tabs-data"><span class="ui-icon ui-icon-folder-open tab-icon"></span> Dane</a></li>
          <li><a href="#tabs-settings"><span class="ui-icon ui-icon-wrench tab-icon"></span> Ustawienia</a></li>
        </ul>
        <div id="tabs-home">
          <apply template="statistics" />
        </div>
        <div id="tabs-data">
          <apply template="data" />
        </div>
        <div id="tabs-settings">
          <div id="settings-accordion">
            <apply template="settings" />
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
