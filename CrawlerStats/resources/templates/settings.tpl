<div>
  <h3><a href="#">Połączenie z serwerem crawlerd</a></h3>
  <div>
    <center>
      <table>
        <tr data-bind="visible: !serverIsConnecting()">
          <td>Adres serwera crawlerd:</td>
          <td><input type="text" name="" data-bind="value: serverAddress" /></td>
        </tr>
        <tr data-bind="visible: serverIsConnecting">
          <td colspan="2"><center><div style="height: 40px;" data-bind="spinVisible: serverIsConnecting()"></div></center></td>
        </tr>
        <tr data-bind="visible: !serverIsConnecting()">
          <td colspan="2"><input type="submit" name="" value="Połącz" style="width: 100%;" data-bind="click: tryConnectServer" /></td>
        </tr>
        <tr>
          <td>Status serwera:</td>
          <td class="listing-value"><span class="ui-icon ui-icon-alert tab-icon" data-bind="visible: serverStatus() == 'Brak połączenia'"></span><span  data-bind="text: serverStatus, style: { color: serverStatus() == 'Brak połączenia' ? '' : (serverStatus() == 'Serwer nie jest uruchomiony' ? 'red' : 'green') }"></span></td>
        </tr>
      </table>
    </center>
  </div>
</div>