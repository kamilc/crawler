module Paths_CrawlerStats (
    version,
    getBinDir, getLibDir, getDataDir, getLibexecDir,
    getDataFileName
  ) where

import Data.Version (Version(..))
import System.Environment (getEnv)

version :: Version
version = Version {versionBranch = [0,1], versionTags = []}

bindir, libdir, datadir, libexecdir :: FilePath

bindir     = "/home/kam/.cabal/bin"
libdir     = "/home/kam/.cabal/lib/CrawlerStats-0.1/ghc-7.0.2"
datadir    = "/home/kam/.cabal/share/CrawlerStats-0.1"
libexecdir = "/home/kam/.cabal/libexec"

getBinDir, getLibDir, getDataDir, getLibexecDir :: IO FilePath
getBinDir = catch (getEnv "CrawlerStats_bindir") (\_ -> return bindir)
getLibDir = catch (getEnv "CrawlerStats_libdir") (\_ -> return libdir)
getDataDir = catch (getEnv "CrawlerStats_datadir") (\_ -> return datadir)
getLibexecDir = catch (getEnv "CrawlerStats_libexecdir") (\_ -> return libexecdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
