{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses, FunctionalDependencies, UndecidableInstances #-}
module Rpc.Server (
  RpcMethod,
  RpcMethodType(..),
  fun,
  serve,
  ) where

--import a
import Control.Monad.Reader

import Control.Applicative
import Control.Concurrent
import Control.DeepSeq
import Control.Exception as E
import Data.Enumerator
import Data.Enumerator.Binary
import Data.Attoparsec.Enumerator
import qualified Data.ByteString.Lazy as BL
import Data.Maybe
import Data.MessagePack
import Network
import System.IO

import Prelude hiding (catch)

bufferSize :: Integer
bufferSize = 4096

type RpcMethod a = [Object] -> ReaderT a IO Object

class RpcMethodType f a | f -> a where
  toRpcMethod :: f -> RpcMethod a

instance OBJECT o => RpcMethodType (ReaderT a IO o) a where
  toRpcMethod m = \[] -> toObject <$> m

instance (OBJECT o, RpcMethodType r a) => RpcMethodType (o -> r) a where
  toRpcMethod f = \(x:xs) -> toRpcMethod (f $! fromObject' x) xs

fromObject' :: OBJECT o => Object -> o
fromObject' o =
  case tryFromObject o of
    Left err -> error $ "argument type error: " ++ err
    Right r -> r

--

-- | Create a RPC method from a Haskell function.
fun :: RpcMethodType f a => f -> RpcMethod a
fun = toRpcMethod


-- | Start RPC server with a set of RPC methods.
serve :: Int -- ^ Port number
         -> [(String, RpcMethod a)] -- ^ list of (method name, RPC method)
         -> ReaderT a IO ()
serve port methods = do
  app <- ask
  liftIO $ withSocketsDo $ do
    runReaderT withSocketsCode app
  where
    --withSocketsCode :: ReaderT a IO ()
    withSocketsCode = do
      app <- ask
      sock <- liftIO $ listenOn (PortNumber $ fromIntegral port)
      forever $ do
        (h, host, hostport) <- liftIO $ accept sock
        liftIO $ forkIO $
          (processRequests h app `finally` hClose h) `catches`
          [ Handler $ \e ->
             case e of
               ParseError ["demandInput"] _ -> return ()
               _ -> hPutStrLn stderr $ host ++ ":" ++ show hostport ++ ": " ++ show e
          , Handler $ \e ->
             hPutStrLn stderr $ host ++ ":" ++ show hostport ++ ": " ++ show (e :: SomeException)]

    processRequests h app =
      run_ $ enumHandle bufferSize h $$ forever $ processRequest h app
    
    processRequest h app = do
      (rtype, msgid, method, args) <- iterParser get
      liftIO $ do
        resp <- try $ getResponse rtype method args app
        case resp of
          Left err ->
            BL.hPutStr h $ pack (1 :: Int, msgid :: Int, show (err :: SomeException), ())
          Right ret ->
            BL.hPutStr h $ pack (1 :: Int, msgid :: Int, (), ret)
        hFlush h

    getResponse rtype method args app = do
      when (rtype /= (0 :: Int)) $
        fail "request type is not 0"
      
      r <- callMethod (method :: String) (args :: [Object]) app
      r `deepseq` return r
    
    callMethod methodName args app =
      case lookup methodName methods of
        Nothing ->
          fail $ "method '" ++ methodName ++ "' not found"
        Just method ->
          runReaderT (method args) app

