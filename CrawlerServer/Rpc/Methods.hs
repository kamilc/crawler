{-# LANGUAGE OverloadedStrings, FlexibleContexts #-}
module Rpc.Methods where

import CrawlerServer
import Rpc.Server
import Control.Monad.Reader
import Control.Concurrent.STM
import Data.DateTime
import Data.List (find)
import CrawlerContracts
import qualified Data.ByteString.UTF8 as UTF8

import Data.Maybe (fromMaybe, fromJust)
import Data.Text.Encoding
import Data.Text (Text, pack)
import Codec.Binary.UTF8.String

rpcMethods :: WorkDomainLike d ServerAction => [(String, RpcMethod (DomainContext d))]
rpcMethods = [ ("nextJob", fun nextJob)
             , ("returnJob", fun returnJob)
             , ("currentStageName", fun currentStageName)
             ]

rpcStatsMethods :: [(String, RpcMethod CrawlerServer)]
rpcStatsMethods = [("serverVersion", fun serverVersion),
                   ("serverDomainNames", fun serverDomainNames),
                   ("serverDomainInfo", fun serverDomainInfo)]

serverVersion :: ReaderT CrawlerServer IO String
serverVersion = return "0.1"

serverDomainNames :: ReaderT CrawlerServer IO [String]
serverDomainNames = do
  tstats <- ask >>= return . serverDomainsStats
  stats <- liftIO $ atomically $ readTVar tstats
  return $ map (UTF8.toString . stDomainName) stats

serverDomainInfo :: String -> ReaderT CrawlerServer IO [String]
serverDomainInfo domainName = do
  app <- ask
  let tstats = serverDomainsStats app
  stats <- liftIO $ atomically $ readTVar tstats
  let stat = fromJust $ find (\s -> stDomainName s == (UTF8.fromString domainName)) stats
  stageName <- liftIO $ atomically $ readTVar $ stStageName stat
  countDone <- stCountDone stat
  countAll <- stCountAll stat
  return [domainName, (UTF8.toString stageName), show countDone, show countAll, "0"]

-- | Zwraca identyfikator aktualnej fazy prac
currentStageName :: WorkDomainLike d ServerAction => ReaderT (DomainContext d) IO WorkerValue
currentStageName = do
  tryTransitionStage
  return . wsName =<< getCurrentStage
  where
    tryTransitionStage = do
      isdone <- isStageDone
      case isdone of
        True -> transitionToNextStage >> tryTransitionStage
        False -> return ()

-- | Zwraca kolejne słowo z listy słów do odwiedzenia
-- @clientId@ jest unikalnym identyfikatorem klienta po którym
-- serwer wpisuje zwracane słowo na listę zajętych
-- Jeśli klient nie odpowie w przeciągu zadanego czasu 
-- odpowiedzi na to słowo to serwer usunie to słowo z
-- listy zajętych i będzie mógł je zaserwować innemu klientowi.
nextJob :: WorkDomainLike d ServerAction => WorkerId -> ReaderT (DomainContext d) IO (Maybe WorkerValue)
nextJob clientId = do
  tryTransitionStage
  tWorkList <- return . ctxWorkList =<< ask
  tProgressList <- return . ctxProgressList =<< ask
  now <- liftIO getCurrentTime
  ret <- do
    workList <- liftIO $ atomically $ readTVar tWorkList
    progressList <- liftIO $ atomically $ readTVar tProgressList
    case workList of
      [] -> do
        return Nothing
      (x:xs) -> do
        inP <- isInProgress progressList x
        case inP of
          False -> do
            liftIO $ atomically $ writeTVar tWorkList xs
            liftIO $ atomically $ writeTVar tProgressList ((InProgress clientId now x):progressList)
            return $ Just x
          True -> do
            liftIO $ atomically $ writeTVar tWorkList xs
            nextJob clientId
  case ret of
       Nothing -> do
          done <- isStageDone
          when done transitionToNextStage
          return Nothing
       Just _ -> do
         liftIO $ putStrLn $ "Podaję zadanie: " ++ (UTF8.toString (fromMaybe "" ret))
         return $ ret
  where
    isInProgress progressList x = do
      case find (\el -> ipJobItem el == x) progressList of
        Nothing -> return False
        Just _ -> return True
    tryTransitionStage = do
      isdone <- isStageDone
      case isdone of
        True -> transitionToNextStage >> tryTransitionStage
        False -> return ()

-- | Dopisuje linki @links@ dla zapytania przetwarzanego przez
-- workera identyfikowanego przez @clienId@. W tym wypadki linki zostają
-- zapisane w bazie. Zapisana zostaje również w bazie informacja że
-- dane słowo zostało już przetworzone. Dodatkowo słowo zostaje
-- zdjęte z list słów oraz zajętych zadań.
returnJob :: WorkDomainLike d ServerAction => WorkerId -> [WorkerValue] -> ReaderT (DomainContext d) IO ()
returnJob clientId retList = do
  --liftIO $ putStrLn (show retList)
  tProgressList <- return . ctxProgressList =<< ask
  stage <- getCurrentStage -- return . wdCurrentStage =<< return . ctxDomain =<< ask
  app <- return . ctxApp =<< ask
  mjob <- liftIO $ atomically $ do
    progressList <- readTVar tProgressList
    writeTVar tProgressList $ filter (\ip -> ipClientId ip /= clientId) progressList
    return $ find (\ip -> ipClientId ip == clientId) progressList
  case mjob of
    Nothing -> do
      liftIO $ putStrLn "mjob = nothing"
      return ()
    Just job -> do
      liftIO $ putStrLn $ "Zapisuję dla zadania:" ++ (show $ ipJobItem job)
      liftIO $ runReaderT ((wsSaveJobResults stage) clientId (ipJobItem job) retList) app
  updateActivityLog
  done <- isStageDone
  when done transitionToNextStage

updateActivityLog :: WorkDomainLike d ServerAction => ReaderT (DomainContext d) IO ()
updateActivityLog = do
  ctx <- ask
  let domainName = wdName $ ctxDomain ctx
  let tstats = serverDomainsStats $ ctxApp ctx
  stats <- liftIO $ atomically $ readTVar tstats
  let stat = fromJust $ find (\s -> stDomainName s == domainName) stats
  let tLog = stLog stat
  now <- liftIO $ getCurrentTime
  liftIO $ atomically $ do
    logs <- readTVar tLog
    writeTVar tLog $ reverse $ take 1000 (now:logs)

-- | Wartość logiczna mówiąca o tym czy dwie listy prac są juz puste
isStageDone :: WorkDomainLike d ServerAction => ReaderT (DomainContext d) IO Bool
isStageDone = do
  ctx <- ask
  liftIO $ atomically $ do
    progresslist <- readTVar $ ctxProgressList ctx
    case null progresslist of
      True -> do
        worklist <- readTVar $ ctxWorkList ctx
        return $ null worklist
      False -> return False

-- | Akcja pobierająca następny WorkStage na podstawie obecnego i jeśli jest to
--   WorkStage różny od obecnego - inicjalizacja go
transitionToNextStage :: WorkDomainLike d ServerAction => ReaderT (DomainContext d) IO ()
transitionToNextStage = do
  ctx <- ask
  server <- return . ctxApp =<< ask
  domena <- return . ctxDomain =<< ask
  tstagename <- return . ctxStageName =<< ask
  stagename <- liftIO $ atomically $ readTVar tstagename
  case null $ UTF8.toString $ stagename of
      False -> do
        liftIO $ atomically $ writeTVar tstagename $ UTF8.fromString ""
        let stage = head $ filter (\s -> wsName s == stagename) $ wdStages domena
        let nextstage = (wdNextStage domena) stage
        let initAction = (wdInitStage domena) nextstage $ ctxWorkList ctx
        liftIO $ runReaderT initAction server
        liftIO $ atomically $ writeTVar tstagename $ wsName nextstage
      True -> return ()
