module EmbedStr (embedStr) where

import Language.Haskell.TH
import Language.Haskell.TH.Syntax (lift)

embedStr :: IO String -> ExpQ
embedStr readStr = lift =<< runIO readStr