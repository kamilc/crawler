{-# LANGUAGE OverloadedStrings, MultiParamTypeClasses, TypeSynonymInstances, FlexibleContexts, FlexibleInstances, FunctionalDependencies, UndecidableInstances #-}

module CrawlerServer (
    CrawlerServer (..)
    , defaultCrawlerServer
    , getCurrentStage
    , getCurrentDomainStage
    , ServerAction
    , DomainContext (..)
    , InProgress (..)
    , DomainStateContext(..)
  , module Prelude
) where

import Prelude hiding (lines)
import qualified Database.MongoDB as MongoDB
import Data.DateTime
import Control.Monad.Reader
import Control.Concurrent
import Control.Concurrent.STM
import CrawlerContracts
import Data.UString (UString)
import qualified Data.ByteString.UTF8 as UTF8

-- DATA ------------------------------------

data CrawlerServer = CrawlerServer { 
  connPool :: MongoDB.ConnPool MongoDB.Host,
  dbName   :: UString,
  serverDomainsStats :: TVar [DomainStateContext]
}

data InProgress = InProgress {
  ipClientId :: WorkerId,
  ipStartedAt :: DateTime,
  ipJobItem :: WorkerValue
}

data WorkDomainLike d ServerAction => DomainContext d = DomainContext { 
  ctxApp :: CrawlerServer,
  ctxStageName :: TVar WorkStageId,
  ctxDomain :: d,
  ctxWorkList :: TVar [WorkerValue],
  ctxProgressList :: TVar [InProgress]
}

data DomainStateContext = DomainStateContext {
  stDomainName :: UTF8.ByteString,
  stStageName :: TVar WorkStageId,
  stLog :: TVar [DateTime],
  stCountDone :: ReaderT CrawlerServer IO Integer,
  stCountAll :: ReaderT CrawlerServer IO Integer
}

defaultCrawlerServer :: IO CrawlerServer
defaultCrawlerServer = do
  pool <- MongoDB.newConnPool 1 $ MongoDB.host "localhost"
  tstats <- atomically $ newTVar []
  return $ CrawlerServer {
    connPool= pool,
    dbName= "crawler",
    serverDomainsStats= tstats
    }

type ServerAction = ReaderT CrawlerServer IO

getCurrentDomainStage :: WorkDomainLike d ServerAction => DomainContext d -> ServerAction (WorkStage ServerAction)
getCurrentDomainStage ctx = do
  let domena = ctxDomain ctx
  let tstagename = ctxStageName ctx
  stagename <- liftIO $ atomically $ readTVar tstagename
  case null $ UTF8.toString stagename of
    True -> do
      liftIO $ threadDelay $ 1000000
      getCurrentDomainStage ctx
    False -> return $ head $ filter (\s -> wsName s == stagename) $ wdStages domena

getCurrentStage :: WorkDomainLike d ServerAction => ReaderT (DomainContext d) IO (WorkStage ServerAction)
getCurrentStage = do
  domena <- return . ctxDomain =<< ask
  tstagename <- return . ctxStageName =<< ask
  stagename <- liftIO $ atomically $ readTVar tstagename
  case null $ UTF8.toString $ stagename of
      False -> return $ head $ filter (\s -> wsName s == stagename) $ wdStages domena
      True -> do
        liftIO $ threadDelay $ 1000000
        getCurrentStage
