{-# LANGUAGE OverloadedStrings, FlexibleContexts #-}

module Main where

import CrawlerServer

import Control.Monad.Reader
import Control.Concurrent
import Control.Concurrent.STM
import Rpc.Server
import Rpc.Methods
import Domains
import Data.ByteString.UTF8 as UTF8
import CrawlerContracts
import Data.DateTime

main :: IO ()
main = runReaderT runCrawlerServer =<< defaultCrawlerServer

-- | Bierze wszystkie domeny i pokolei inicjalizuje dla nich servery RPC
runCrawlerServer :: ServerAction ()
runCrawlerServer = do
  runDomainAsync =<< getGoogleDomain
  runDomainAsync =<< getGooglePagesDomain
  runDomainAsync =<< getGoogleMailsDomain
  runDomainAsync =<< getCustomGoogleDomain
  runDomainAsync =<< getFirmyNetDomain
  runDomainAsync =<< getFirmyNetPagesDomain
  runDomainAsync =<< getFirmyNetMailsDomain
  runDomainAsync =<< getHermesDomain
  runDomainAsync =<< getCatalogsDomain
  runStats
  waitForCtrlC
  where
    waitForCtrlC = do
      liftIO $ threadDelay $ 1000000
      waitForCtrlC
    runDomainAsync domain = do
      app <- ask
      _ <- liftIO $ forkIO $ runReaderT (runDomainRpc domain) app
      return ()

runDomainRpc :: WorkDomainLike d ServerAction => d -> ServerAction ()
runDomainRpc d = do
  app <- ask
  list <- liftIO $ atomically $ newTVar []
  progress <- liftIO $ atomically $ newTVar []
  firststagename <- liftIO $ atomically $ newTVar $ wsName $ head $ wdStages d
  let domena = DomainContext app firststagename d list progress
  initializeDomain domena
  _ <- liftIO $ forkIO $ runCleaner domena
  liftIO $ putStrLn $ "Domena \"" ++ (UTF8.toString $ wdName d) ++ "\" startuje na porcie " ++ (show $ wdPort d)
  liftIO $ runReaderT (serve (wdPort d) rpcMethods) $ domena

initializeDomain :: WorkDomainLike d ServerAction => DomainContext d -> ServerAction ()
initializeDomain ctx = do
  stage <- getCurrentDomainStage ctx
  initStage stage $ ctxWorkList ctx
  tstats <- ask >>= return . serverDomainsStats
  stats <- liftIO $ atomically $ readTVar tstats
  tlog <- liftIO $ atomically $ newTVar []
  let curStat = DomainStateContext (wdName $ ctxDomain ctx) (ctxStageName ctx) tlog (wsCountDone stage) (wsCountAll stage)
  liftIO $ atomically $ writeTVar tstats (curStat:stats)
  where
    domain = ctxDomain ctx
    initStage = wdInitStage domain
    
runCleaner :: WorkDomainLike d ServerAction => DomainContext d -> IO ()
runCleaner ctx = do
  threadDelay $ 1000000*60*30
  now <- getCurrentTime
  atomically $ do
    plist <- readTVar tplist
    writeTVar tplist $ filter (notAbandoned now) plist
  runCleaner ctx
  where
    tplist = ctxProgressList ctx
    notAbandoned now progress = (diffMinutes now (ipStartedAt progress)) > 5

runStats :: ServerAction ()
runStats = do
  serve 1905 rpcStatsMethods
