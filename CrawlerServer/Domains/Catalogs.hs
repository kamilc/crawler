{-# LANGUAGE OverloadedStrings, TemplateHaskell #-}
module Domains.Catalogs where

import CrawlerServer
import CrawlerContracts
import Contracts.Catalogs
import DataAccess
import Control.Monad.Reader
import Control.Concurrent
import Control.Concurrent.STM
import Data.List hiding (insert)
import Data.Text.Encoding
import qualified Database.MongoDB as MongoDB
import Massive.Database.MongoDB
import Data.Maybe (fromMaybe)
import Control.Concurrent (threadDelay)
import Data.Time.Clock
import Model
import EmbedStr
import Database.MongoDB.Admin
import Data.Bson
import qualified Data.ByteString.UTF8 as UTF8

getCatalogsDomain :: ServerAction (CatalogsDomain ServerAction)
getCatalogsDomain = return $ ((CatalogsDomain  [(getCatalogsGetPagesStage :: WorkStage ServerAction ),
                                                (getCatalogsWorkPageStage :: WorkStage ServerAction)] catalogsInit) )

catalogsInit :: WorkStage ServerAction -> TVar [WorkerValue] -> ServerAction ()
catalogsInit stage twlist = catalogsInitPages stage twlist

catalogsInitPages :: WorkStage ServerAction -> TVar [WorkerValue] -> ServerAction ()
catalogsInitPages _ tworklist = do
  let all_phrases = UTF8.lines $ UTF8.fromString customFile
  edone_phrases <- getDoneCatalogs
  case edone_phrases of
    Left err -> do
      liftIO $ putStrLn $ show err
    Right done_phrases -> do
      liftIO $ atomically $ do
        writeTVar tworklist $ all_phrases \\ (map (\p -> encodeUtf8 $ catalogUrl p) done_phrases)
  where
    customFile = $(embedStr $ readFile "katalogi.txt")

getCatalogsGetPagesStage :: WorkStage ServerAction
getCatalogsGetPagesStage = defaultCatalogsGetPages {
  wsSaveJobResults= returnCatalogsGetPages,
  wsCountDone= return 0,
  wsCountAll= return 0
  }

getCatalogsWorkPageStage :: WorkStage ServerAction
getCatalogsWorkPageStage = defaultCatalogsWorkPage {
  wsSaveJobResults= returnCatalogsWorkPage,
  wsCountDone= return 0,
  wsCountAll= return 0
  }

-------------------------------------------------------------------------------

returnCatalogsGetPages :: WorkerId -> WorkerValue -> [WorkerValue] -> ServerAction ()
returnCatalogsGetPages _ url links = do
  saveCatalogResults links url
  updateCatalogProcessed url

returnCatalogsWorkPage :: WorkerId -> WorkerValue -> [WorkerValue] -> ServerAction ()
returnCatalogsWorkPage _ url links = undefined

-------------------------------------------------------------------------------

saveCatalogResults :: [WorkerValue] -> WorkerValue -> ServerAction ()
saveCatalogResults links word
  | null links = return ()
  | otherwise = mapM (\l -> saveCatalogResult l) links >> return ()

saveCatalogResult :: WorkerValue -> ServerAction ()
saveCatalogResult link = do
  let linkAddress = CatalogResult $ decodeUtf8 link
  ekey <- runDB $ insert $ linkAddress
  case ekey of
    Left failure -> waitAndTryAgain failure
    Right _ -> return ()
  where
    waitAndTryAgain failure = do
      liftIO $ threadDelay $ 1000000
      liftIO $ putStrLn $ "Wystąpił bład podczas komunikacji z bazą danych. Próbuję ponownie."
      liftIO $ putStrLn $ "Treść błędu: " ++ (show failure)
      saveCatalogResult link

updateCatalogProcessed :: WorkerValue -> ServerAction ()
updateCatalogProcessed  word = do
  ec <- runDB $ count [CatalogUrl `eq` (decodeUtf8 word)]
  case ec of
    Left f -> waitAndTryAgain f
    Right c -> do
      case c == 0 of
        True -> do
          now <- liftIO $ getCurrentTime
          let gword = Catalog (decodeUtf8 word) (Just now)
          ekey <- runDB $ insert gword
          case ekey of
            Left f -> waitAndTryAgain f
            Right _ -> return ()
        False -> do
          now <- liftIO $ getCurrentTime
          esuccess <- runDB $ updateWhere [CatalogUrl `eq` (decodeUtf8 word)] [CatalogProcessedAt `set` (Just now)]
          case esuccess of
            Left f -> waitAndTryAgain f
            Right _ -> return ()
  where
    waitAndTryAgain f = do
      liftIO $ threadDelay $ 1000000
      liftIO $ putStrLn $ "Wystąpił bład podczas komunikacji z bazą danych przy próbie uaktualnienia słowa. Próbuję ponownie."
      liftIO $ putStrLn $ "Treść błędu: " ++ (show f)
      updateCatalogProcessed word

-------------------------------------------------------------------------------

getDoneCatalogs :: ServerAction (Either MongoDB.Failure [Catalog])
getDoneCatalogs = do
  ewords <- runDB $ select [CatalogProcessedAt `eq` Nothing]
  case ewords of
       Left err -> return $ Left err
       Right _words -> return $ Right $ map snd _words

-------------------------------------------------------------------------------