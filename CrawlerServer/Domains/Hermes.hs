{-# LANGUAGE OverloadedStrings, DoAndIfThenElse #-}
module Domains.Hermes where

import CrawlerServer
import CrawlerContracts
import Contracts.Hermes
import DataAccess
import Control.Monad.Reader
import Control.Concurrent.STM
import Control.Concurrent
import Data.Text.Encoding
import Massive.Database.MongoDB
import Control.Concurrent (threadDelay)
import Model
import Data.DateTime
import Data.List (nub)
import qualified Data.Text as T
import qualified Data.ByteString.UTF8 as UTF8

getHermesDomain :: ServerAction (HermesDomain ServerAction)
getHermesDomain = return $ ((HermesDomain  [(getIndustriesStage :: WorkStage ServerAction ),
                                            (getCardsStage :: WorkStage ServerAction ),
                                            (getCompaniesStage :: WorkStage ServerAction )] hermesInit) )

getIndustriesStage :: WorkStage ServerAction
getIndustriesStage = defaultHermesGetIndustries {
  wsSaveJobResults= hermesIndustriesReturnJob,
  wsCountDone= return 0,
  wsCountAll= return 0
  }

getCardsStage :: WorkStage ServerAction
getCardsStage = defaultHermesGetCompanyCards {
  wsSaveJobResults= hermesCardsReturnJob,
  wsCountDone= return 0,
  wsCountAll= return 0
  }

getCompaniesStage :: WorkStage ServerAction
getCompaniesStage = defaultHermesGetCompanies {
  wsSaveJobResults= hermesCompaniesReturnJob,
  wsCountDone= return 0,
  wsCountAll= return 0
  }

hermesIndustriesReturnJob :: WorkerId -> WorkerValue -> [WorkerValue] -> ServerAction ()
hermesIndustriesReturnJob _ url industries =
  if not $ null industries then do
    saveIndustries industries
    markLetterDone url
  else do
    markLetterDone url

hermesCardsReturnJob :: WorkerId -> WorkerValue -> [WorkerValue] -> ServerAction ()
hermesCardsReturnJob _ url cards = do
  saveCards cards
  markIndustryDone url

hermesCompaniesReturnJob :: WorkerId -> WorkerValue -> [WorkerValue] -> ServerAction ()
hermesCompaniesReturnJob _ url [""] = do
  markCardDone url

hermesCompaniesReturnJob _ url company = do
  saveCompany company
  markCardDone url

hermesInit :: WorkStage ServerAction -> TVar [WorkerValue] -> ServerAction ()
hermesInit stage twlist = 
  case UTF8.toString $ wsName stage of
       "GetIndustries" -> industryHermesLinksInit twlist
       "GetCompanyCards" -> companyHermesLinksInit twlist
       "GetCompanies" -> companiesHermesInit twlist
       other -> error $ "Nie zaimplementowano inicjalizacji stage'a domeny FirmyNet o nazwie " ++ other
       
industryHermesLinksInit :: TVar [WorkerValue] -> ServerAction ()
industryHermesLinksInit twlist = do
  ecount <- runDB $ count [HermesLetterUrl `ne` ""]
  case ecount of
    Left _ -> do
      liftIO $ threadDelay 100000
      industryHermesLinksInit twlist
    Right _count -> do
      case _count == 0 of
        True -> mapM_ createHermesLetter ("abcćdeęfghijklłmnoóprsśtuwzźż" :: String)
        False -> return ()
      eletters <- runDB $ select [HermesLetterProcessedAt `eq` Nothing]
      case eletters of
        Left _ -> do
          liftIO $ threadDelay 100000
          industryHermesLinksInit twlist
        Right letters -> do
          liftIO $ atomically $ do
            writeTVar twlist $ map (\l -> encodeUtf8 $ hermesLetterUrl $ snd l) letters
  where
    createHermesLetter :: Char -> ServerAction ()
    createHermesLetter _letter = do
      ekey <- runDB $ insert $ HermesLetter (T.pack $ "http://firmy.wp.pl/branze/?lt=" ++ [_letter]) Nothing
      case ekey of
        Left _ -> do
          liftIO $ threadDelay 100000
          createHermesLetter _letter
        Right _ -> return ()

companyHermesLinksInit :: TVar [WorkerValue] -> ServerAction ()
companyHermesLinksInit twlist = do
  eindustries <- runDB $ select [HermesIndustryProcessedAt `eq` Nothing]
  case eindustries of
    Left _ -> do
      liftIO $ threadDelay 100000
      companyHermesLinksInit twlist
    Right industries -> do
      liftIO $ atomically $ do
        writeTVar twlist $ map (\i -> encodeUtf8 $ hermesIndustryUrl $ snd i) industries

companiesHermesInit :: TVar [WorkerValue] -> ServerAction ()
companiesHermesInit twlist = do
  ecards <- runDB $ selectLimit [HermesCompanyProcessedAt `eq` Nothing] 0 500
  case ecards of
    Left _ -> do
      liftIO $ threadDelay 100000
      companiesHermesInit twlist
    Right cards -> do
      liftIO $ atomically $ do
        list <- readTVar twlist
        writeTVar twlist $ nub $ (map (encodeUtf8 . hermesCompanyUrl . snd) cards) ++ list
      context <- ask
      liftIO $ forkIO $ runReaderT (do
        liftIO $ threadDelay $ 1000000*60*5 -- po pięciu minutach znowu
        companiesHermesInit twlist >> return ()) context
      return ()

-- MONGODB ---------------------------------------------------------------------
saveIndustries :: [WorkerValue] -> ServerAction ()
saveIndustries industries = do
  mapM_ saveIndustry industries
  where
    saveIndustry industry = do
      case industry == "" of
        True -> return ()
        False -> do
          emind <- runDB $ selectOne [HermesIndustryUrl `eq` (decodeUtf8 industry)]
          case emind of
            Left _ -> do
              liftIO $ threadDelay 100000
              saveIndustry industry
            Right mind -> do
              case mind of
                Just _ -> return ()
                Nothing -> do
                  ekey <- runDB $ insert $ HermesIndustry (decodeUtf8 industry) Nothing
                  case ekey of
                    Left _ -> do
                      liftIO $ threadDelay 100000
                      saveIndustry industry
                    Right _ -> return ()

markLetterDone :: WorkerValue -> ServerAction ()
markLetterDone url = do
  now <- liftIO $ getCurrentTime
  eres <- runDB $ updateWhere [HermesLetterUrl `eq` (decodeUtf8 url)] [HermesLetterProcessedAt `set` (Just now)]
  case eres of
    Left _ -> do
      liftIO $ threadDelay 100000
      markLetterDone url
    Right _ -> return ()

saveCards :: [WorkerValue] -> ServerAction ()
saveCards cards = do
  mapM_ saveCard cards
  where
    saveCard card = do
      emcard <- runDB $ selectOne [HermesCompanyUrl `eq` (decodeUtf8 card)]
      case emcard of
        Left _ -> do
          liftIO $ threadDelay 100000
          saveCard card
        Right mcard -> do
          case mcard of
            Just _ -> return ()
            Nothing -> do
              ekey <- runDB $ insert $ HermesCompanyCardUrl (decodeUtf8 card) Nothing
              case ekey of
                Left _ -> do
                  liftIO $ threadDelay 100000
                  saveCard card
                Right _ -> return ()

markIndustryDone :: WorkerValue -> ServerAction ()
markIndustryDone url = do
  now <- liftIO $ getCurrentTime
  eres <- runDB $ updateWhere [HermesIndustryUrl `eq` (decodeUtf8 url)] [HermesIndustryProcessedAt `set` (Just now)]
  case eres of
    Left _ -> do
      liftIO $ threadDelay 100000
      markIndustryDone url
    Right _ -> return ()

markCardDone :: WorkerValue -> ServerAction ()
markCardDone url = do
  now <- liftIO $ getCurrentTime
  eres <- runDB $ updateWhere [HermesCompanyUrl `eq` (decodeUtf8 url)] [HermesCompanyProcessedAt `set` (Just now)]
  case eres of
    Left _ -> do
      liftIO $ threadDelay 100000
      markCardDone url
    Right _ -> return ()

saveCompany :: [WorkerValue] -> ServerAction ()
saveCompany company = do -- [nazwa, card_url, www, email, telefon, miasto]
  case null (UTF8.toString $ company !! 0 ) of
    True -> return ()
    False -> do
      ccount <- runDB $ count [HermesCompanyName `eq` (decodeUtf8 $ company !! 0)]
      case ccount of
        Left _ -> do
          liftIO $ threadDelay 100000
          saveCompany company
        Right _count -> do
          case _count == 0 of
            False -> return ()
            True -> do
              ekey <- runDB $ insert $ HermesCompany {
                hermesCompanyName = decodeUtf8 $  company !! 0,
                hermesCompanyPostalCode = Just $ decodeUtf8 $  company !! 1,
                hermesCompanyCity = Just $ decodeUtf8 $  company !! 2,
                hermesCompanyStreet = Just $ decodeUtf8 $  company !! 3,
                hermesCompanyDistrict = Just $ decodeUtf8 $  company !! 4,
                hermesCompanyPhone = Just $ decodeUtf8 $  company !! 5,
                hermesCompanyWww = Just $ decodeUtf8 $  company !! 6,
                hermesCompanyEmail = Just $ decodeUtf8 $  company !! 7
                }
              case ekey of
                Left _ -> do
                  liftIO $ threadDelay 100000
                  saveCompany company
                Right _ -> return ()

