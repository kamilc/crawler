{-# LANGUAGE OverloadedStrings #-}
module Domains.Google where

import CrawlerServer
import CrawlerContracts
import Contracts.Google
import DataAccess
import Control.Monad.Reader
import Control.Concurrent.STM
import Data.List hiding (insert)
import Data.Text.Encoding
import qualified Database.MongoDB as MongoDB
import Massive.Database.MongoDB
import Data.Maybe (fromMaybe)
import Control.Concurrent (threadDelay)
import Data.Time.Clock
import Model
import Database.MongoDB.Admin
import Data.Bson

import qualified Data.ByteString as B
import qualified Data.ByteString.UTF8 as UTF8

getGoogleDomain :: ServerAction (GoogleDomain ServerAction)
getGoogleDomain = return $ ((GoogleDomain  [(getGoogleStage :: WorkStage ServerAction )] googleInit) )

getGoogleStage :: WorkStage ServerAction
getGoogleStage = defaultGoogleGetLinks {
  wsSaveJobResults= googleReturnJob,
  wsCountDone= googleCountDone,
  wsCountAll= googleCountAll
  }

googleCountDone :: ServerAction Integer
googleCountDone = do
  edone <- runDB $ count $ [WordWord `ne` ""]
  case edone of
    Left _ -> do
      liftIO $ threadDelay 1000000
      googleCountDone
    Right done -> return $ fromIntegral done

googleCountAll :: ServerAction Integer
googleCountAll = return 3495123

googleReturnJob :: WorkerId -> WorkerValue -> [WorkerValue] -> ServerAction ()
googleReturnJob _ word links = do
  saveLinks links word
  updateWordVisited word

googleInit :: WorkStage ServerAction -> TVar [WorkerValue] -> ServerAction ()
googleInit _ tworklist = do
  _ <- runDB $ createIndex $ index "LinkAddress" ["linkUrl" =: (1 :: Int)]
  _ <- runDB $ createIndex $ index "GoogleWord" ["wordWord" =: (1 :: Int)]
  all_phrases <- liftIO $ (B.readFile $ "/usr/share/dict/polish") >>= return . UTF8.lines
  edone_phrases <- getDoneGooglePhrases
  case edone_phrases of
    Left err -> do
      liftIO $ putStrLn $ show err
    Right done_phrases -> do
      liftIO $ atomically $ do
        writeTVar tworklist $ all_phrases \\ (map (\p -> encodeUtf8 $ wordWord p) done_phrases)
        --writeTVar tworklist $ allWithoutDone all_phrases $! (map (\p -> encodeUtf8 $ wordWord p) done_phrases)
  where
    allWithoutDone :: [UTF8.ByteString] -> [UTF8.ByteString] -> [UTF8.ByteString]
    allWithoutDone all_phrases done_phrases =
      allWithoutDone' all_phrases done_phrases []

    allWithoutDone' [] done_phrases ret = ret
    allWithoutDone' (x:xs) done_phrases ret = allWithoutDone' xs done_phrases $! ret ++ (matches x done_phrases)
      where
        matches x done = 
          case x `elem` done of
            True -> [x]
            False -> []

-- MONGODB ---------------------------------------------------------------------

sequence :: Monad m => [m a] -> m [a]
sequence ms = reverse `liftM` sequence' [] ms

sequence' l [] = return l
sequence' l (m:ms) = m >>= \x -> sequence' (x:l) ms

getDoneGooglePhrases_native :: ServerAction (Either MongoDB.Failure [UTF8.ByteString])
getDoneGooglePhrases_native = do
  ewords <- runDB $ MongoDB.find ((MongoDB.select [] "GoogleWord"){ MongoDB.project = [(u "wordWord" =: (1 :: Int))] }) >>= MongoDB.rest
  case ewords of
    Left err -> return $ Left err
    Right ws -> return $ Right $ map (UTF8.fromString . MongoDB.at (u "wordWord")) $ Domains.Google.sequence ws

getDoneGooglePhrases :: ServerAction (Either MongoDB.Failure [GoogleWord])
getDoneGooglePhrases = do
  ewords <- runDB $ select [WordWord `ne` ""]
  case ewords of
       Left err -> return $ Left err
       Right _words -> return $ Right $ map snd _words
       
saveLinks :: [UTF8.ByteString] -> UTF8.ByteString -> ReaderT CrawlerServer IO ()
saveLinks links word
  | null links = return ()
  | otherwise = mapM (\l -> saveLink l word (fromMaybe 0 $ elemIndex l links)) links >> return ()

saveLink :: UTF8.ByteString -> UTF8.ByteString -> Int -> ReaderT CrawlerServer IO ()
saveLink link word rank = do
  ec <- runDB $ count [LinkUrl `eq` (decodeUtf8 link)]
  case ec of
    Left failure -> waitAndTryAgain failure
    Right c -> do
      case c == 0 of
        True -> do
          now <- liftIO $ getCurrentTime
          let linkAddress = LinkAddress { 
               linkUrl= decodeUtf8 link
              , linkCreatedAt= now
              , linkUpdatedAt= now
              , linkWords= [(decodeUtf8 word, rank)]
              , linkProcessedAt= Nothing
              , linkHtml= Nothing
              , linkEmails= []
              }
          ekey <- runDB $ insert $ linkAddress
          case ekey of
            Left failure -> waitAndTryAgain failure
            Right _ -> return ()
        False -> updateLink link word rank
  where
    waitAndTryAgain failure = do
      liftIO $ threadDelay $ 1000000
      liftIO $ putStrLn $ "Wystąpił bład podczas komunikacji z bazą danych. Próbuję ponownie."
      liftIO $ putStrLn $ "Treść błędu: " ++ (show failure)
      saveLink link word rank

updateLink :: UTF8.ByteString -> UTF8.ByteString -> Int -> ReaderT CrawlerServer IO ()
updateLink link word rank = do
  esuccess <- runDB $ updateWhere [LinkUrl `eq` (decodeUtf8 link)] [LinkWords `addToSet` (decodeUtf8 word, rank)]
  case esuccess of
    Left f -> do
      liftIO $ threadDelay $ 1000000
      liftIO $ putStrLn $ "Wystąpił bład podczas komunikacji z bazą danych przy próbie uaktualnienia linku. Próbuję ponownie."
      liftIO $ putStrLn $ "Treść błędu: " ++ (show f)
      updateLink link word rank
    Right _ -> return ()
      
updateWordVisited :: UTF8.ByteString -> ReaderT CrawlerServer IO ()
updateWordVisited word = do
  ec <- runDB $ count [WordWord `eq` (decodeUtf8 word)]
  case ec of
    Left f -> waitAndTryAgain f
    Right c -> do
      case c == 0 of
        True -> do
          now <- liftIO $ getCurrentTime
          let gword = GoogleWord {
                                  wordWord= decodeUtf8 word
                                 , wordProcessedAt= Just now
                                 }
          ekey <- runDB $ insert gword 
          case ekey of
            Left f -> waitAndTryAgain f
            Right _ -> return ()
        False -> do
          now <- liftIO $ getCurrentTime
          esuccess <- runDB $ updateWhere [WordWord `eq` (decodeUtf8 word)] [WordProcessedAt `set` (Just now)]
          case esuccess of
            Left f -> waitAndTryAgain f
            Right _ -> return ()
  where
    waitAndTryAgain f = do
      liftIO $ threadDelay $ 1000000
      liftIO $ putStrLn $ "Wystąpił bład podczas komunikacji z bazą danych przy próbie uaktualnienia słowa. Próbuję ponownie."
      liftIO $ putStrLn $ "Treść błędu: " ++ (show f)
      updateWordVisited word