{-# LANGUAGE OverloadedStrings, TemplateHaskell #-}
module Domains.Allegro where

import CrawlerServer
import CrawlerContracts
import Contracts.Allegro
import DataAccess
import Control.Monad.Reader
import Control.Concurrent
import Control.Concurrent.STM
import Data.List hiding (insert)
import Data.Text.Encoding
import qualified Database.MongoDB as MongoDB
import Massive.Database.MongoDB
import Data.Maybe (fromMaybe)
import Control.Concurrent (threadDelay)
import Data.Time.Clock
import Model
import EmbedStr
import Database.MongoDB.Admin
import Data.Bson
import qualified Data.ByteString.UTF8 as UTF8

getAllegroDomain :: ServerAction (AllegroDomain ServerAction)
getAllegroDomain = return $ ((AllegroDomain  [(getAllegroGetCatalogsStage :: WorkStage ServerAction ),
                                              (getAllegroGetSubCatalogsStage :: WorkStage ServerAction)] allegroInit) )

allegroInit :: WorkStage ServerAction -> TVar [WorkerValue] -> ServerAction ()
allegroInit stage twlist = allegroInitCatalogs stage twlist

allegroInitCatalogs :: WorkStage ServerAction -> TVar [WorkerValue] -> ServerAction ()
allegroInitCatalogs _ tworklist = undefined

getAllegroGetCatalogsStage :: WorkStage ServerAction
getAllegroGetCatalogsStage = defaultAllegroGetCategories {
  wsSaveJobResults= returnAllegroCategories,
  wsCountDone= return 0,
  wsCountAll= return 0
  }

getAllegroGetSubCatalogsStage :: WorkStage ServerAction
getAllegroGetSubCatalogsStage = defaultAllegroGetSubCategories {
  wsSaveJobResults= returnAllegroSubCategories,
  wsCountDone= return 0,
  wsCountAll= return 0
  }

-------------------------------------------------------------------------------

returnAllegroCategories :: WorkerId -> WorkerValue -> [WorkerValue] -> ServerAction ()
returnAllegroCategories _ url links = do
  saveCatalogResults links url
  updateCatalogProcessed url

returnAllegroSubCategories :: WorkerId -> WorkerValue -> [WorkerValue] -> ServerAction ()
returnAllegroSubCategories _ url links = undefined

-------------------------------------------------------------------------------

saveCatalogResults :: [WorkerValue] -> WorkerValue -> ServerAction ()
saveCatalogResults links url
  | null links = return ()
  | otherwise = mapM (\l -> saveCatalogResult l) links >> return ()

saveCatalogResult :: WorkerValue -> ServerAction ()
saveCatalogResult link = do
  let linkAddress = CatalogResult $ decodeUtf8 link
  ekey <- runDB $ insert $ linkAddress
  case ekey of
    Left failure -> waitAndTryAgain failure
    Right _ -> return ()
  where
    waitAndTryAgain failure = do
      liftIO $ threadDelay $ 1000000
      liftIO $ putStrLn $ "Wystąpił bład podczas komunikacji z bazą danych. Próbuję ponownie."
      liftIO $ putStrLn $ "Treść błędu: " ++ (show failure)
      saveCatalogResult link

updateCatalogProcessed :: WorkerValue -> ServerAction ()
updateCatalogProcessed  word = undefined