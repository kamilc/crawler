{-# LANGUAGE OverloadedStrings #-}
module Domains.FirmyNet where

import CrawlerServer
import Contracts.FirmyNet
import Control.Concurrent.STM.TVar
import qualified Data.ByteString.UTF8 as UTF8
import Massive.Database.MongoDB
import Control.Monad.Reader
import Control.Concurrent
import Control.Concurrent.STM
import DataAccess
import Model
import Data.Text.Encoding
import CrawlerContracts
import Data.DateTime
import Data.Text (Text, pack)
import Database.MongoDB.Admin
import Data.Bson
import Data.List (nub)

-- Akcje Return ---------------------------------------------
returnIndustries :: WorkerId -> WorkerValue -> [WorkerValue] -> ServerAction ()
returnIndustries _ _ links = do
  saveIndustryLinks links

returnCompanyLinks :: WorkerId -> WorkerValue -> [WorkerValue] -> ServerAction ()
returnCompanyLinks _ industryUrl links = do
  saveCompanyLinks links
  markIndustryProcessed industryUrl

returnCompany :: WorkerId -> WorkerValue -> [WorkerValue] -> ServerAction ()
returnCompany _ companyCardUrl [""] = markCompanyDone companyCardUrl
returnCompany _ companyCardUrl companyData = do
  updateCompany companyCardUrl companyData

-- Baza danych ----------------------------------------------
saveIndustryLinks :: [WorkerValue] -> ServerAction()
saveIndustryLinks links = mapM_ saveIndustryLinkPack links
  where 
    saveIndustryLinkPack :: WorkerValue -> ServerAction ()
    saveIndustryLinkPack stplurl = do
      let tplurl = read $ UTF8.toString stplurl :: (String, Int)
      mapM_ saveIndustryLink $ 
        map (\i -> pack $ fst tplurl ++ "?p=" ++ (show i)) [1..((ceiling $ (fromIntegral $ snd tplurl) / (15.0 :: Double)) :: Integer)]
    saveIndustryLink :: Text -> ServerAction ()
    saveIndustryLink url = do
      ret <- runDB $ insert $ FirmyNetIndustry {
        firmyIndustryUrl= url,
        firmyIndustryProcessedAt= Nothing
        }
      case ret of
        Left _ -> do
          liftIO $ threadDelay 1000000
          saveIndustryLink url
        Right _ -> return ()

saveCompanyLinks :: [WorkerValue] -> ServerAction()
saveCompanyLinks links = do
  mapM_ saveCompanyCard links
  where 
    saveCompanyCard url = do
      ecount <- runDB $ count [FirmyCompanyCardUrl `eq` (decodeUtf8 url)]
      case ecount of
        Left err -> do
          liftIO $ putStrLn $ show err
          liftIO $ threadDelay 100000
          saveCompanyCard url
        Right _count -> do
          case _count == 0 of
            False -> return ()
            True -> do
              ekey <- runDB $ insert $ FirmyNetCompany {
                firmyCompanyCardUrl= decodeUtf8 url,
                firmyCompanyName= Nothing,
                firmyCompanyPostalCode= Nothing,
                firmyCompanyStreet= Nothing,
                firmyCompanyAddress= Nothing,
                firmyCompanyCity= Nothing,
                firmyCompanyDistrict= Nothing,
                firmyCompanyPhone= Nothing,
                firmyCompanyWWW= Nothing,
                firmyCompanyEmails= [],
                firmyCompanyProcessedAt= Nothing,
                firmyCompanyWWWFetchedAt = Nothing
                }
              case ekey of
                Left err -> do
                  liftIO $ putStrLn $ show err
                  liftIO $ threadDelay 1000000
                  saveCompanyCard url
                Right _ -> return ()

markIndustryProcessed :: WorkerValue -> ServerAction()
markIndustryProcessed industryUrl = do
  now <- liftIO $ getCurrentTime
  eres <- runDB $ updateWhere [FirmyIndustryUrl `eq` (decodeUtf8 industryUrl)] [FirmyIndustryProcessedAt `set` (Just now)]
  case eres of
    Left err -> do
      liftIO $ putStrLn $ show err
      liftIO $ threadDelay 100000
      markIndustryProcessed industryUrl
    Right _ -> return ()

markCompanyDone :: WorkerValue -> ServerAction()
markCompanyDone cardUrl = do
  ecompany <- runDB $ selectOne [FirmyCompanyCardUrl `eq` (decodeUtf8 cardUrl)]
  case ecompany of
    Left _ -> do
      liftIO $ threadDelay 100000
      markCompanyDone cardUrl
    Right mcompany -> do
      case mcompany of
        Nothing -> return ()
        Just company -> do
          now <- liftIO $ getCurrentTime
          ekey <- runDB $ update (fst company) [
            FirmyCompanyProcessedAt  `set` (Just now)
            ]
          case ekey of
            Left _ -> do
              liftIO $ threadDelay 100000
              markCompanyDone cardUrl
            Right _ -> return ()

updateCompany :: WorkerValue -> [WorkerValue] -> ServerAction()
updateCompany cardUrl companyData = do
  ecompany <- runDB $ selectOne [FirmyCompanyCardUrl `eq` (decodeUtf8 cardUrl)]
  case ecompany of
    Left _ -> do
      liftIO $ threadDelay 100000
      updateCompany cardUrl companyData
    Right mcompany -> do
      case mcompany of
        Nothing -> return ()
        Just company -> do
          now <- liftIO $ getCurrentTime
          ekey <- runDB $ update (fst company) [ 
            FirmyCompanyName         `set` (Just $ decodeUtf8 $ companyData !! 0),
            FirmyCompanyPostalCode   `set` (Just $ decodeUtf8 $ companyData !! 1),
            FirmyCompanyCity         `set` (Just $ decodeUtf8 $ companyData !! 2),
            FirmyCompanyStreet       `set` (Just $ decodeUtf8 $ companyData !! 3),
            FirmyCompanyDistrict     `set` (Just $ decodeUtf8 $ companyData !! 4),
            FirmyCompanyPhone        `set` (Just $ decodeUtf8 $ companyData !! 5),
            FirmyCompanyWWW          `set` (Just $ decodeUtf8 $ companyData !! 6),
            FirmyCompanyProcessedAt  `set` (Just now)
            ]
          case ekey of
            Left _ -> do
              liftIO $ threadDelay 100000
              updateCompany cardUrl companyData
            Right _ -> return ()

-- Domena ---------------------------------------------------
getFirmyNetDomain :: ServerAction (FirmyNetDomain ServerAction)
getFirmyNetDomain = return $ ((FirmyNetDomain  [(getFirmyIndustryLinksStage :: WorkStage ServerAction ),
                                                (getFirmyCompanyLinksStage  :: WorkStage ServerAction ),
                                                (getFirmyCompaniesStage     :: WorkStage ServerAction )] firmyInit))

getFirmyIndustryLinksStage :: WorkStage ServerAction
getFirmyIndustryLinksStage = defaultFirmyGetIndustryLinks {
  wsSaveJobResults= returnIndustries,
  wsCountDone= return 0,
  wsCountAll= return 0
}

getFirmyCompanyLinksStage :: WorkStage ServerAction
getFirmyCompanyLinksStage = defaultFirmyGetCompanyLinks {
  wsSaveJobResults= returnCompanyLinks,
  wsCountDone= return 0,
  wsCountAll= return 0
}

getFirmyCompaniesStage :: WorkStage ServerAction
getFirmyCompaniesStage = defaultFirmyGetCompanies {
  wsSaveJobResults= returnCompany,
  wsCountDone= return 0,
  wsCountAll= return 0
}

firmyInit :: WorkStage ServerAction -> TVar [WorkerValue] -> ServerAction ()
firmyInit stage twlist = do
  _ <- runDB $ createIndex $ index "FirmyIndustry"   ["firmyIndustryUrl" =: (1 :: Int)]
  _ <- runDB $ createIndex $ index "FirmyNetCompany" ["firmyCompanyCardUrl" =: (1 :: Int)]
  _ <- runDB $ createIndex $ index "FirmyNetCompany" ["firmyCompanyProcessedAt" =: (1 :: Int)]
  case UTF8.toString $ wsName stage of
       "GetIndustryLinks" -> industryLinksInit twlist
       "GetCompanyLinks" -> companyLinksInit twlist
       "GetCompanies" -> companiesInit twlist
       other -> error $ "Nie zaimplementowano inicjalizacji stage'a domeny FirmyNet o nazwie " ++ other
       
industryLinksInit :: TVar [WorkerValue] -> ServerAction ()
industryLinksInit twlist = do
  ecount <- runDB $ count [FirmyIndustryUrl `ne` ""]
  case ecount of
       Left err -> do
	 liftIO $ putStrLn $ show err
	 liftIO $ threadDelay 100000
	 industryLinksInit twlist
       Right _count -> do
	 case _count == 0 of
           False -> do
	     liftIO $ atomically $ writeTVar twlist []
	   True -> do
	     liftIO $ atomically $ writeTVar twlist ["http://www.firmy.net/katalog.html"]

-- | Bierze jako placeholder tvar listy prac, odczytuje z bazy wszystkie linki
--   do podstron kategorii i zapisuje w liscie prac te wszystkie urle jako liste
companyLinksInit :: TVar [WorkerValue] -> ServerAction ()
companyLinksInit twlist = do
  eindustries <- runDB $ select [FirmyIndustryProcessedAt `eq` Nothing]
  case eindustries of
    Left err -> do
      liftIO $ putStrLn $ show err
      liftIO $ threadDelay 100000
      companyLinksInit twlist
    Right industries -> do
      liftIO $ atomically $ writeTVar twlist $ map (\i -> encodeUtf8 $ firmyIndustryUrl $ snd i) industries

companiesInit :: TVar [WorkerValue] -> ServerAction ()
companiesInit twlist = do
  ecompanies <- runDB $ selectLimit [FirmyCompanyProcessedAt `eq` Nothing] 0 500
  case ecompanies of
    Left err -> do
      liftIO $ putStrLn $ show err
      liftIO $ threadDelay 100000
      companiesInit twlist
    Right companies -> do
      processListToTVar companies
      context <- ask
      case null companies of
        True -> return ()
        False -> do
          liftIO $ forkIO $ runReaderT (do
            liftIO $ threadDelay $ 1000000*60*5 -- po pięciu minutach znowu
            companiesInit twlist >> return ()) context
          return ()
  where
    processListToTVar :: [(FirmyNetCompanyId, FirmyNetCompany)] -> ServerAction ()
    processListToTVar companies =
      case companies of
        [] -> return ()
        ((_, company):rest) -> do
          list <- liftIO $ atomically $ readTVar $ twlist
          let url = encodeUtf8 $ firmyCompanyCardUrl $ company
          liftIO $ atomically $ writeTVar twlist $ nub $ (url:list)
          processListToTVar rest