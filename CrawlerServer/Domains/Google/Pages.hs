{-# LANGUAGE OverloadedStrings #-}
module Domains.Google.Pages where

import CrawlerServer
import Contracts.Google.Pages
import Control.Concurrent.STM.TVar
import Massive.Database.MongoDB
import Control.Monad.Reader
import Control.Concurrent
import Control.Concurrent.STM
import DataAccess
import Model
import Data.Text.Encoding
import CrawlerContracts
import Data.List (nub)

-- Domena ---------------------------------------------------
getGooglePagesDomain :: ServerAction (GooglePagesDomain ServerAction)
getGooglePagesDomain = return $ ((GooglePagesDomain  [(getGooglePagesStage :: WorkStage ServerAction )] googlePagesPagesInit))

getGooglePagesStage :: WorkStage ServerAction
getGooglePagesStage = defaultGooglePagesGetPages {
  wsSaveJobResults= saveGoogleLinkPage,
  wsCountDone= return 0,
  wsCountAll= return 0
}

saveGoogleLinkPage :: WorkerId -> WorkerValue -> [WorkerValue] -> ServerAction ()
saveGoogleLinkPage cid url htmls = do
  eret <- runDB $ updateWhere [LinkUrl `eq` (decodeUtf8 url)] [LinkHtml `set` (Just $ decodeUtf8 $ head htmls)]
  case eret of
    Left _ -> do
      liftIO $ threadDelay 100000
      saveGoogleLinkPage cid url htmls
    Right _ -> return ()

googlePagesPagesInit :: WorkStage ServerAction -> TVar [WorkerValue] -> ServerAction ()
googlePagesPagesInit stage twlist = do
  elinks <- runDB $ selectLimit [LinkProcessedAt `eq` Nothing] 0 500
  case elinks of
    Left _ -> do
      liftIO $ threadDelay 100000
      googlePagesPagesInit stage twlist
    Right links -> do
      liftIO $ atomically $ do
        list <- readTVar twlist
        writeTVar twlist $ nub $ (map (encodeUtf8 . linkUrl . snd) links) ++ list
      context <- ask
      liftIO $ forkIO $ runReaderT (do
        liftIO $ threadDelay $ 1000000*60*5 -- po pięciu minutach znowu
        googlePagesPagesInit stage twlist >> return ()) context
      return ()