{-# LANGUAGE OverloadedStrings #-}
module Domains.Google.Mails where

import CrawlerServer
import Contracts.Google.Mails
import Control.Concurrent.STM.TVar
import Massive.Database.MongoDB
import Control.Monad.Reader
import Control.Concurrent
import Control.Concurrent.STM
import DataAccess
import Model
import Data.Text.Encoding
import CrawlerContracts
import Data.Maybe (fromJust)
import Data.List (nub)

-- Domena ---------------------------------------------------
getGoogleMailsDomain :: ServerAction (GoogleMailsDomain ServerAction)
getGoogleMailsDomain = return $ ((GoogleMailsDomain  [(getGoogleMailsStage :: WorkStage ServerAction )] googleMailsInit))

getGoogleMailsStage :: WorkStage ServerAction
getGoogleMailsStage = defaultGoogleGetMails {
  wsSaveJobResults= saveGoogleEmails,
  wsCountDone= return 0,
  wsCountAll= return 0
}

saveGoogleEmails :: WorkerId -> WorkerValue -> [WorkerValue] -> ServerAction ()
saveGoogleEmails cid html emails = do
  eret <- runDB $ updateWhere [LinkHtml `eq` (Just $ decodeUtf8 html)] [LinkEmails `addManyToSet` (map decodeUtf8 emails)]
  case eret of
    Left _ -> do
      liftIO $ threadDelay 100000
      saveGoogleEmails cid html emails
    Right _ -> return ()

googleMailsInit :: WorkStage ServerAction -> TVar [WorkerValue] -> ServerAction ()
googleMailsInit stage twlist = do
  elinks <- runDB $ selectLimit [LinkEmails `eq` [], LinkHtml `ne` Nothing] 0 500
  case elinks of
    Left _ -> do
      liftIO $ threadDelay 100000
      googleMailsInit stage twlist
    Right links -> do
      liftIO $ atomically $ do
        list <- readTVar twlist
        writeTVar twlist $ nub $ (map (encodeUtf8 . fromJust . linkHtml . snd) links) ++ list
      context <- ask
      liftIO $ forkIO $ runReaderT (do
        liftIO $ threadDelay $ 1000000*60*5 -- po pięciu minutach znowu
        googleMailsInit stage twlist >> return ()) context
      return ()