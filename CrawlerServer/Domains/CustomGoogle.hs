{-# LANGUAGE OverloadedStrings, TemplateHaskell #-}
module Domains.CustomGoogle where

import CrawlerServer
import CrawlerContracts
import Contracts.CustomGoogle
import DataAccess
import Control.Monad.Reader
import Control.Concurrent.STM
import Data.List hiding (insert)
import Data.Text.Encoding
import qualified Database.MongoDB as MongoDB
import Massive.Database.MongoDB
import Data.Maybe (fromMaybe)
import Control.Concurrent (threadDelay)
import Data.Time.Clock
import Model
import EmbedStr
import Database.MongoDB.Admin
import Data.Bson
import qualified Data.ByteString.UTF8 as UTF8


getCustomGoogleDomain :: ServerAction (CustomGoogleDomain ServerAction)
getCustomGoogleDomain = return $ ((CustomGoogleDomain  [(getCustomGoogleStage :: WorkStage ServerAction )] customGoogleInit) )

getCustomGoogleStage :: WorkStage ServerAction
getCustomGoogleStage = defaultCustomGoogleGetLinks {
  wsSaveJobResults= customGoogleReturnJob,
  wsCountDone= return 0,
  wsCountAll= return 0
  }

customGoogleReturnJob :: WorkerId -> WorkerValue -> [WorkerValue] -> ServerAction ()
customGoogleReturnJob _ word links = do
  saveCustomLinks links word
  updateCustomWordVisited word

customGoogleInit :: WorkStage ServerAction -> TVar [WorkerValue] -> ServerAction ()
customGoogleInit _ tworklist = do
  _ <- runDB $ createIndex $ index "CustomLinkAddress" ["customLinkUrl" =: (1 :: Int)]
  _ <- runDB $ createIndex $ index "CustomGoogleWord" ["customWord" =: (1 :: Int)]
  let all_phrases = UTF8.lines $ UTF8.fromString customFile
  edone_phrases <- getDoneCustomGooglePhrases
  case edone_phrases of
    Left err -> do
      liftIO $ putStrLn $ show err
    Right done_phrases -> do
      liftIO $ atomically $ do
        writeTVar tworklist $ all_phrases \\ (map (\p -> encodeUtf8 $ customWord p) done_phrases)
  where
    customFile = $(embedStr $ readFile "customWords.txt")

-- MONGODB ---------------------------------------------------------------------
getDoneCustomGooglePhrases :: ServerAction (Either MongoDB.Failure [CustomGoogleWord])
getDoneCustomGooglePhrases = do
  ewords <- runDB $ select [CustomWord `ne` ""]
  case ewords of
       Left err -> return $ Left err
       Right _words -> return $ Right $ map snd _words
       
saveCustomLinks :: [UTF8.ByteString] -> UTF8.ByteString -> ReaderT CrawlerServer IO ()
saveCustomLinks links word
  | null links = return ()
  | otherwise = mapM (\l -> saveCustomLink l word (fromMaybe 0 $ elemIndex l links)) links >> return ()

saveCustomLink :: UTF8.ByteString -> UTF8.ByteString -> Int -> ReaderT CrawlerServer IO ()
saveCustomLink link word rank = do
  ec <- runDB $ count [CustomLinkUrl `eq` (decodeUtf8 link)]
  case ec of
    Left failure -> waitAndTryAgain failure
    Right c -> do
      case c == 0 of
        True -> do
          now <- liftIO $ getCurrentTime
          let linkAddress = CustomLinkAddress { 
               customLinkUrl= decodeUtf8 link
              , customLinkCreatedAt= now
              , customLinkUpdatedAt= now
              , customLinkWords= [(decodeUtf8 word, rank)]
              }
          ekey <- runDB $ insert $ linkAddress
          case ekey of
            Left failure -> waitAndTryAgain failure
            Right _ -> return ()
        False -> updateCustomLink link word rank
  where
    waitAndTryAgain failure = do
      liftIO $ threadDelay $ 1000000
      liftIO $ putStrLn $ "Wystąpił bład podczas komunikacji z bazą danych. Próbuję ponownie."
      liftIO $ putStrLn $ "Treść błędu: " ++ (show failure)
      saveCustomLink link word rank

updateCustomLink :: UTF8.ByteString -> UTF8.ByteString -> Int -> ReaderT CrawlerServer IO ()
updateCustomLink link word rank = do
  esuccess <- runDB $ updateWhere [CustomLinkUrl `eq` (decodeUtf8 link)] [CustomLinkWords `addToSet` (decodeUtf8 word, rank)]
  case esuccess of
    Left f -> do
      liftIO $ threadDelay $ 1000000
      liftIO $ putStrLn $ "Wystąpił bład podczas komunikacji z bazą danych przy próbie uaktualnienia linku. Próbuję ponownie."
      liftIO $ putStrLn $ "Treść błędu: " ++ (show f)
      updateCustomLink link word rank
    Right _ -> return ()
      
updateCustomWordVisited :: UTF8.ByteString -> ReaderT CrawlerServer IO ()
updateCustomWordVisited word = do
  ec <- runDB $ count [CustomWord `eq` (decodeUtf8 word)]
  case ec of
    Left f -> waitAndTryAgain f
    Right c -> do
      case c == 0 of
        True -> do
          now <- liftIO $ getCurrentTime
          let gword = CustomGoogleWord {
                                  customWord= decodeUtf8 word
                                 , customWordProcessedAt= Just now
                                 }
          ekey <- runDB $ insert gword 
          case ekey of
            Left f -> waitAndTryAgain f
            Right _ -> return ()
        False -> do
          now <- liftIO $ getCurrentTime
          esuccess <- runDB $ updateWhere [CustomWord `eq` (decodeUtf8 word)] [CustomWordProcessedAt `set` (Just now)]
          case esuccess of
            Left f -> waitAndTryAgain f
            Right _ -> return ()
  where
    waitAndTryAgain f = do
      liftIO $ threadDelay $ 1000000
      liftIO $ putStrLn $ "Wystąpił bład podczas komunikacji z bazą danych przy próbie uaktualnienia słowa. Próbuję ponownie."
      liftIO $ putStrLn $ "Treść błędu: " ++ (show f)
      updateCustomWordVisited word
