{-# LANGUAGE OverloadedStrings #-}
module Domains.FirmyNet.Mails where

import CrawlerServer
import Contracts.FirmyNet.Mails
import Control.Concurrent.STM.TVar
import Massive.Database.MongoDB
import Control.Monad.Reader
import Control.Concurrent
import Control.Concurrent.STM
import DataAccess
import Model
import Data.Text.Encoding
import CrawlerContracts
import Data.Maybe (fromJust)
import Data.DateTime

-- Domena ---------------------------------------------------
getFirmyNetMailsDomain :: ServerAction (FirmyNetMailsDomain ServerAction)
getFirmyNetMailsDomain = return $ ((FirmyNetMailsDomain  [(getFirmyMailsStage :: WorkStage ServerAction )] firmyMailsInit))

getFirmyMailsStage :: WorkStage ServerAction
getFirmyMailsStage = defaultFirmyGetMails {
  wsSaveJobResults= firmyMailsSave,
  wsCountDone= return 0,
  wsCountAll= return 0
}

firmyMailsSave :: WorkerId -> WorkerValue -> [WorkerValue] -> ServerAction ()
firmyMailsSave _ url mails = do
  saveMails url mails
  markPageProcessed url

markPageProcessed :: WorkerValue -> ServerAction ()
markPageProcessed url = do
  now <- liftIO $  getCurrentTime
  eret <- runDB $ updateWhere [FirmyNetPageUrl `eq` (decodeUtf8 url)] [FirmyNetPageProcessedAt `set` (Just now)]
  case eret of
    Left _ -> do
      liftIO $ threadDelay 100000
      markPageProcessed url
    Right _ -> return ()

saveMails :: WorkerValue -> [WorkerValue] -> ServerAction ()
saveMails url mails = do
  emcompany <- runDB $ selectOne [FirmyCompanyWWW `eq` (Just $ decodeUtf8 url)]
  case emcompany of
    Left _ -> do
      liftIO $ threadDelay 100000
      saveMails url mails
    Right mcompany -> do
      case mcompany of
        Nothing -> return ()
        Just _ -> do
          eret <- runDB $ updateWhere [FirmyCompanyWWW `eq` (Just $ decodeUtf8 url)] [FirmyCompanyEmails `addManyToSet` (map decodeUtf8 mails)]
          case eret of
            Left _ -> do
              liftIO $ threadDelay 100000
              saveMails url mails
            Right _ -> return ()

firmyMailsInit :: WorkStage ServerAction -> TVar [WorkerValue] -> ServerAction ()
firmyMailsInit stage twlist = do
  epages <- runDB $ select [FirmyNetPageProcessedAt `eq` Nothing]
  case epages of
    Left _ -> do
      liftIO $ threadDelay 100000
      firmyMailsInit stage twlist
    Right pages -> do
      liftIO $ atomically $ do
        writeTVar twlist $ map (encodeUtf8 . fromJust . firmyNetPageHtml . snd) pages