{-# LANGUAGE OverloadedStrings #-}
module Domains.FirmyNet.Pages where

import CrawlerServer
import Contracts.FirmyNet.Pages
import Control.Concurrent.STM.TVar
import qualified Data.ByteString.UTF8 as UTF8
import Massive.Database.MongoDB
import Control.Monad.Reader
import Control.Concurrent
import Control.Concurrent.STM
import DataAccess
import Model
import Data.Text.Encoding
import CrawlerContracts
import Data.DateTime
import Data.Maybe (fromMaybe)

-- Domena ---------------------------------------------------
getFirmyNetPagesDomain :: ServerAction (FirmyNetPagesDomain ServerAction)
getFirmyNetPagesDomain = return $ ((FirmyNetPagesDomain  [(getFirmyPagesStage :: WorkStage ServerAction )] firmyPagesInit))

getFirmyPagesStage :: WorkStage ServerAction
getFirmyPagesStage = defaultFirmyGetPages {
  wsSaveJobResults= firmyPagesSave,
  wsCountDone= return 0,
  wsCountAll= return 0
}

firmyPagesSave :: WorkerId -> WorkerValue -> [WorkerValue] -> ServerAction ()
firmyPagesSave wid url htmls = do
  mcompany_t <- getCompanyByUrl url
  case mcompany_t of
    Nothing -> do
      case (take 2 $ reverse $ UTF8.toString url) == "/." of
        True -> firmyPagesSave wid (UTF8.fromString $ take ((length $ UTF8.toString url) - 2) $ UTF8.toString url) htmls
        False -> return ()
    Just company_t -> do
      mapM_ (saveCompanyPage (fst company_t) url) htmls
      now <- liftIO $ getCurrentTime
      runDB $ update (fst company_t) [FirmyCompanyWWWFetchedAt `set` (Just now)]
      return ()
  where
    getCompanyByUrl :: UTF8.ByteString -> ServerAction (Maybe (FirmyNetCompanyId, FirmyNetCompany))
    getCompanyByUrl _url = do
      emcompany_t <- runDB $ selectOne [FirmyCompanyWWW `eq` (Just $ decodeUtf8 _url)]
      case emcompany_t of
        Left _ -> do
          liftIO $ threadDelay 100000
          getCompanyByUrl _url
        Right mcompany_t -> return mcompany_t
    
    saveCompanyPage :: FirmyNetCompanyId -> UTF8.ByteString -> UTF8.ByteString -> ServerAction ()
    saveCompanyPage _id _url _html = do
      now <- liftIO $ getCurrentTime
      ekey <- runDB $ insert $ FirmyNetPage _id url' html $ Just now
      case ekey of
        Left _ -> do
          liftIO $ threadDelay 100000
          saveCompanyPage _id _url _html
        Right _ -> return ()
      where
        url' = decodeUtf8 _url
        html = Just $ decodeUtf8 _html

-- | Listą zadań są wszystkie www firm już zapisanych w bazie
-- | oraz wszystkie url'e z pages jeszcze nie przetworzonych, ponieważ
-- | pagesy nieprzrtworzone pochodzą z htmli firm
firmyPagesInit :: WorkStage ServerAction -> TVar [WorkerValue] -> ServerAction ()
firmyPagesInit stage twlist = do
  ecompanies_t <- runDB $ selectLimit [(FirmyCompanyWWWFetchedAt `eq` Nothing), (FirmyCompanyWWW `ne` Nothing)] 0 500
  case ecompanies_t of
    Left _ -> do
      liftIO $ threadDelay 100000
      firmyPagesInit stage twlist
    Right companies_t -> do
      eblankpages_t <- runDB $ selectLimit [FirmyNetPageProcessedAt `eq` Nothing] 0 500
      case eblankpages_t of
        Left _ -> do
          liftIO $ threadDelay 100000
          firmyPagesInit stage twlist
        Right blankpages_t -> do
          liftIO $ atomically $ do
            writeTVar twlist $ (wwws companies_t) ++ (blanks blankpages_t)
          context <- ask
          liftIO $ forkIO $ runReaderT (do
            liftIO $ threadDelay $ 1000000*60*5 -- po pięciu minutach znowu
            firmyPagesInit stage twlist >> return ()) context
          return ()
  where
    wwws = map (encodeUtf8 . fromMaybe "" . firmyCompanyWWW . snd)
    blanks = map (encodeUtf8 . firmyNetPageUrl . snd)

