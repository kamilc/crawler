{-# LANGUAGE TemplateHaskell, TypeFamilies, OverloadedStrings, DoAndIfThenElse, TypeSynonymInstances #-}
module Model where

import Massive.Database.MongoDB
import Massive.Database.MongoDB.Template
import Data.Time.Clock
import Data.Text
import Control.Applicative
import Data.Bson

data GoogleWord = GoogleWord {
                              wordWord :: Text
                             , wordProcessedAt :: Maybe UTCTime
                             }
type GoogleWordId = Key GoogleWord

data LinkAddress = LinkAddress {
                   linkUrl :: Text
                 , linkCreatedAt :: UTCTime
                 , linkUpdatedAt :: UTCTime
                 , linkWords     :: [(Text, Int)]
                 , linkHtml      :: Maybe Text
                 , linkProcessedAt :: Maybe UTCTime
                 , linkEmails    :: [Text]
                 }
type LinkAddressId = Key LinkAddress

data CustomGoogleWord = CustomGoogleWord {
                               customWord :: Text
                             , customWordProcessedAt :: Maybe UTCTime
                             }
type CustomGoogleWordId = Key CustomGoogleWord

data CustomLinkAddress = CustomLinkAddress {
                   customLinkUrl :: Text
                 , customLinkCreatedAt :: UTCTime
                 , customLinkUpdatedAt :: UTCTime
                 , customLinkWords     :: [(Text, Int)]
                 }
type CustomLinkAddressId = Key CustomLinkAddress

data FirmyNetIndustry = FirmyNetIndustry {
                                          firmyIndustryUrl :: Text
                                         , firmyIndustryProcessedAt :: Maybe UTCTime
                                         }
type FirmyNetIndustryId = Key FirmyNetIndustry



data FirmyNetCompany = FirmyNetCompany {
  firmyCompanyCardUrl :: Text,
  firmyCompanyName :: Maybe Text,
  firmyCompanyPostalCode :: Maybe Text,
  firmyCompanyStreet :: Maybe Text,
  firmyCompanyAddress :: Maybe Text,
  firmyCompanyCity :: Maybe Text,
  firmyCompanyDistrict :: Maybe Text,
  firmyCompanyPhone :: Maybe Text,
  firmyCompanyWWW :: Maybe Text,
  firmyCompanyEmails :: [Text],
  firmyCompanyProcessedAt :: Maybe UTCTime,
  firmyCompanyWWWFetchedAt :: Maybe UTCTime
} deriving (Show)
type FirmyNetCompanyId = Key FirmyNetCompany

data FirmyNetPage = FirmyNetPage {
  firmyNetPageCompanyId :: FirmyNetCompanyId,
  firmyNetPageUrl :: Text,
  firmyNetPageHtml :: Maybe Text,
  firmyNetPageProcessedAt :: Maybe UTCTime
}

type FirmyNetPageId = Key FirmyNetPage

data HermesCompany = HermesCompany {
  hermesCompanyName :: Text,
  hermesCompanyPostalCode :: Maybe Text,
  hermesCompanyCity :: Maybe Text,
  hermesCompanyStreet :: Maybe Text,
  hermesCompanyDistrict :: Maybe Text,
  hermesCompanyWww :: Maybe Text,
  hermesCompanyEmail :: Maybe Text,
  hermesCompanyPhone :: Maybe Text
}

type HermesCompanyId = Key HermesCompany

data HermesLetter = HermesLetter {
  hermesLetterUrl :: Text,
  hermesLetterProcessedAt :: Maybe UTCTime
}

type HermesLetterId = Key HermesLetter

data HermesIndustry = HermesIndustry {
  hermesIndustryUrl :: Text,
  hermesIndustryProcessedAt :: Maybe UTCTime
}

type HermesIndustryId = Key HermesIndustry

data HermesCompanyCardUrl = HermesCompanyCardUrl {
  hermesCompanyUrl :: Text,
  hermesCompanyProcessedAt :: Maybe UTCTime
}

type HermesCompanyCardUrlId = Key HermesCompanyCardUrl

data Catalog = Catalog {
  catalogUrl :: Text,
  catalogProcessedAt :: Maybe UTCTime
}

type CatalogId = Key Catalog

data CatalogPage = CatalogPage {
  catalogPageHtml :: Text,
  catalogPageCatalogUrl :: Text,
  catalogPageProcessedAt :: Maybe UTCTime
}

type CatalogPageId = Key CatalogPage

data CatalogResult = CatalogResult {
  catalogResultUrl :: Text
}

type CatalogResultId = Key CatalogResult

$(asMongoEntity ''GoogleWord)
$(asMongoEntity ''LinkAddress)
$(asMongoEntity ''CustomGoogleWord)
$(asMongoEntity ''CustomLinkAddress)
$(asMongoEntity ''FirmyNetIndustry)
$(asMongoEntity ''FirmyNetCompany)
$(asMongoEntity ''FirmyNetPage)
$(asMongoEntity ''HermesCompany)
$(asMongoEntity ''HermesCompanyCardUrl)
$(asMongoEntity ''HermesIndustry)
$(asMongoEntity ''HermesLetter)
$(asMongoEntity ''Catalog)
$(asMongoEntity ''CatalogPage)
$(asMongoEntity ''CatalogResult)