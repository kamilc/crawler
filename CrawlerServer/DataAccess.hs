{-# LANGUAGE OverloadedStrings #-}
module DataAccess where

import CrawlerServer
import Control.Monad.Reader
import qualified Database.MongoDB as MongoDB
import Massive.Database.MongoDB

runDB ::  ReaderT MongoDB.Database (Action (ReaderT CrawlerServer IO)) a -> ReaderT CrawlerServer IO (Either MongoDB.Failure a) 
runDB action = do
  app <- ask
  MongoDB.access MongoDB.safe MongoDB.Master (connPool app) $ MongoDB.use (MongoDB.Database $ dbName app) action